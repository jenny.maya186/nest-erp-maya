import { Injectable } from '@nestjs/common';
import { CreateChatSupportDto } from './dto/create-chat-support.dto';
import { UpdateChatSupportDto } from './dto/update-chat-support.dto';

@Injectable()
export class ChatSupportService {
  create(createChatSupportDto: CreateChatSupportDto) {
    return 'This action adds a new chatSupport';
  }

  findAll() {
    return `This action returns all chatSupport`;
  }

  findOne(id: number) {
    return `This action returns a #${id} chatSupport`;
  }

  update(id: number, updateChatSupportDto: UpdateChatSupportDto) {
    return `This action updates a #${id} chatSupport`;
  }

  remove(id: number) {
    return `This action removes a #${id} chatSupport`;
  }
}
