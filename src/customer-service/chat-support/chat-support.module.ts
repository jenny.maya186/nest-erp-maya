import { Module } from '@nestjs/common';
import { ChatSupportService } from './chat-support.service';
import { ChatSupportGateway } from './chat-support.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ChatSupportGateway, ChatSupportService],
})
export class ChatSupportModule {}
