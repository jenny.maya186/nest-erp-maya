import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ChatSupportService } from './chat-support.service';
import { CreateChatSupportDto } from './dto/create-chat-support.dto';
import { UpdateChatSupportDto } from './dto/update-chat-support.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'chat-support' })
export class ChatSupportGateway extends BaseGateway {
  constructor(
    private readonly chatSupportService: ChatSupportService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createChatSupport')
  create(@MessageBody() createChatSupportDto: CreateChatSupportDto) {
    return this.chatSupportService.create(createChatSupportDto);
  }

  @SubscribeMessage('findAllChatSupport')
  findAll() {
    return this.chatSupportService.findAll();
  }

  @SubscribeMessage('findOneChatSupport')
  findOne(@MessageBody() id: number) {
    return this.chatSupportService.findOne(id);
  }

  @SubscribeMessage('updateChatSupport')
  update(@MessageBody() updateChatSupportDto: UpdateChatSupportDto) {
    return this.chatSupportService.update(
      updateChatSupportDto.id,
      updateChatSupportDto,
    );
  }

  @SubscribeMessage('removeChatSupport')
  remove(@MessageBody() id: number) {
    return this.chatSupportService.remove(id);
  }
}
