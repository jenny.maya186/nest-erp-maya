import { PartialType } from '@nestjs/mapped-types';
import { CreateHelpdeskSupportDto } from './create-helpdesk-support.dto';

export class UpdateHelpdeskSupportDto extends PartialType(CreateHelpdeskSupportDto) {
  id: number;
}
