import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { HelpdeskSupportService } from './helpdesk-support.service';
import { CreateHelpdeskSupportDto } from './dto/create-helpdesk-support.dto';
import { UpdateHelpdeskSupportDto } from './dto/update-helpdesk-support.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'helpdesk-support' })
export class HelpdeskSupportGateway extends BaseGateway {
  constructor(
    private readonly helpdeskSupportService: HelpdeskSupportService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createHelpdeskSupport')
  create(@MessageBody() createHelpdeskSupportDto: CreateHelpdeskSupportDto) {
    return this.helpdeskSupportService.create(createHelpdeskSupportDto);
  }

  @SubscribeMessage('findAllHelpdeskSupport')
  findAll() {
    return this.helpdeskSupportService.findAll();
  }

  @SubscribeMessage('findOneHelpdeskSupport')
  findOne(@MessageBody() id: number) {
    return this.helpdeskSupportService.findOne(id);
  }

  @SubscribeMessage('updateHelpdeskSupport')
  update(@MessageBody() updateHelpdeskSupportDto: UpdateHelpdeskSupportDto) {
    return this.helpdeskSupportService.update(
      updateHelpdeskSupportDto.id,
      updateHelpdeskSupportDto,
    );
  }

  @SubscribeMessage('removeHelpdeskSupport')
  remove(@MessageBody() id: number) {
    return this.helpdeskSupportService.remove(id);
  }
}
