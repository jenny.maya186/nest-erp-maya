import { Module } from '@nestjs/common';
import { HelpdeskSupportService } from './helpdesk-support.service';
import { HelpdeskSupportGateway } from './helpdesk-support.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [HelpdeskSupportGateway, HelpdeskSupportService],
})
export class HelpdeskSupportModule {}
