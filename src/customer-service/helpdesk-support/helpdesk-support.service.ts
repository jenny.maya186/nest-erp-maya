import { Injectable } from '@nestjs/common';
import { CreateHelpdeskSupportDto } from './dto/create-helpdesk-support.dto';
import { UpdateHelpdeskSupportDto } from './dto/update-helpdesk-support.dto';

@Injectable()
export class HelpdeskSupportService {
  create(createHelpdeskSupportDto: CreateHelpdeskSupportDto) {
    return 'This action adds a new helpdeskSupport';
  }

  findAll() {
    return `This action returns all helpdeskSupport`;
  }

  findOne(id: number) {
    return `This action returns a #${id} helpdeskSupport`;
  }

  update(id: number, updateHelpdeskSupportDto: UpdateHelpdeskSupportDto) {
    return `This action updates a #${id} helpdeskSupport`;
  }

  remove(id: number) {
    return `This action removes a #${id} helpdeskSupport`;
  }
}
