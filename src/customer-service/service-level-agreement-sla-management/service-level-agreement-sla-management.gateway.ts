import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ServiceLevelAgreementSlaManagementService } from './service-level-agreement-sla-management.service';
import { CreateServiceLevelAgreementSlaManagementDto } from './dto/create-service-level-agreement-sla-management.dto';
import { UpdateServiceLevelAgreementSlaManagementDto } from './dto/update-service-level-agreement-sla-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'sla' })
export class ServiceLevelAgreementSlaManagementGateway extends BaseGateway {
  constructor(
    private readonly serviceLevelAgreementSlaManagementService: ServiceLevelAgreementSlaManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createServiceLevelAgreementSlaManagement')
  create(
    @MessageBody()
    createServiceLevelAgreementSlaManagementDto: CreateServiceLevelAgreementSlaManagementDto,
  ) {
    return this.serviceLevelAgreementSlaManagementService.create(
      createServiceLevelAgreementSlaManagementDto,
    );
  }

  @SubscribeMessage('findAllServiceLevelAgreementSlaManagement')
  findAll() {
    return this.serviceLevelAgreementSlaManagementService.findAll();
  }

  @SubscribeMessage('findOneServiceLevelAgreementSlaManagement')
  findOne(@MessageBody() id: number) {
    return this.serviceLevelAgreementSlaManagementService.findOne(id);
  }

  @SubscribeMessage('updateServiceLevelAgreementSlaManagement')
  update(
    @MessageBody()
    updateServiceLevelAgreementSlaManagementDto: UpdateServiceLevelAgreementSlaManagementDto,
  ) {
    return this.serviceLevelAgreementSlaManagementService.update(
      updateServiceLevelAgreementSlaManagementDto.id,
      updateServiceLevelAgreementSlaManagementDto,
    );
  }

  @SubscribeMessage('removeServiceLevelAgreementSlaManagement')
  remove(@MessageBody() id: number) {
    return this.serviceLevelAgreementSlaManagementService.remove(id);
  }
}
