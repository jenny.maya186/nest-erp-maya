import { Injectable } from '@nestjs/common';
import { CreateServiceLevelAgreementSlaManagementDto } from './dto/create-service-level-agreement-sla-management.dto';
import { UpdateServiceLevelAgreementSlaManagementDto } from './dto/update-service-level-agreement-sla-management.dto';

@Injectable()
export class ServiceLevelAgreementSlaManagementService {
  create(createServiceLevelAgreementSlaManagementDto: CreateServiceLevelAgreementSlaManagementDto) {
    return 'This action adds a new serviceLevelAgreementSlaManagement';
  }

  findAll() {
    return `This action returns all serviceLevelAgreementSlaManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} serviceLevelAgreementSlaManagement`;
  }

  update(id: number, updateServiceLevelAgreementSlaManagementDto: UpdateServiceLevelAgreementSlaManagementDto) {
    return `This action updates a #${id} serviceLevelAgreementSlaManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} serviceLevelAgreementSlaManagement`;
  }
}
