import { PartialType } from '@nestjs/mapped-types';
import { CreateServiceLevelAgreementSlaManagementDto } from './create-service-level-agreement-sla-management.dto';

export class UpdateServiceLevelAgreementSlaManagementDto extends PartialType(CreateServiceLevelAgreementSlaManagementDto) {
  id: number;
}
