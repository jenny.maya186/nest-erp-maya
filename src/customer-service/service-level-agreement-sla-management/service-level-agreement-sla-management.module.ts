import { Module } from '@nestjs/common';
import { ServiceLevelAgreementSlaManagementService } from './service-level-agreement-sla-management.service';
import { ServiceLevelAgreementSlaManagementGateway } from './service-level-agreement-sla-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ServiceLevelAgreementSlaManagementGateway, ServiceLevelAgreementSlaManagementService],
})
export class ServiceLevelAgreementSlaManagementModule {}
