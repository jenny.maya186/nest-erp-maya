import { Module } from '@nestjs/common';
import { HelpdeskSupportModule } from './helpdesk-support/helpdesk-support.module';
import { ChatSupportModule } from './chat-support/chat-support.module';
import { FeedbackManagementModule } from './feedback-management/feedback-management.module';
import { ServiceLevelAgreementSlaManagementModule } from './service-level-agreement-sla-management/service-level-agreement-sla-management.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [
    HelpdeskSupportModule,
    ChatSupportModule,
    FeedbackManagementModule,
    ServiceLevelAgreementSlaManagementModule,
  ],
})
export class CustomerServiceModule {}
