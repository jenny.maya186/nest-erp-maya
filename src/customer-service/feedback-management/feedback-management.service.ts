import { Injectable } from '@nestjs/common';
import { CreateFeedbackManagementDto } from './dto/create-feedback-management.dto';
import { UpdateFeedbackManagementDto } from './dto/update-feedback-management.dto';

@Injectable()
export class FeedbackManagementService {
  create(createFeedbackManagementDto: CreateFeedbackManagementDto) {
    return 'This action adds a new feedbackManagement';
  }

  findAll() {
    return `This action returns all feedbackManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} feedbackManagement`;
  }

  update(id: number, updateFeedbackManagementDto: UpdateFeedbackManagementDto) {
    return `This action updates a #${id} feedbackManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} feedbackManagement`;
  }
}
