import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { FeedbackManagementService } from './feedback-management.service';
import { CreateFeedbackManagementDto } from './dto/create-feedback-management.dto';
import { UpdateFeedbackManagementDto } from './dto/update-feedback-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'feedback' })
export class FeedbackManagementGateway extends BaseGateway {
  constructor(
    private readonly feedbackManagementService: FeedbackManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createFeedbackManagement')
  create(
    @MessageBody() createFeedbackManagementDto: CreateFeedbackManagementDto,
  ) {
    return this.feedbackManagementService.create(createFeedbackManagementDto);
  }

  @SubscribeMessage('findAllFeedbackManagement')
  findAll() {
    return this.feedbackManagementService.findAll();
  }

  @SubscribeMessage('findOneFeedbackManagement')
  findOne(@MessageBody() id: number) {
    return this.feedbackManagementService.findOne(id);
  }

  @SubscribeMessage('updateFeedbackManagement')
  update(
    @MessageBody() updateFeedbackManagementDto: UpdateFeedbackManagementDto,
  ) {
    return this.feedbackManagementService.update(
      updateFeedbackManagementDto.id,
      updateFeedbackManagementDto,
    );
  }

  @SubscribeMessage('removeFeedbackManagement')
  remove(@MessageBody() id: number) {
    return this.feedbackManagementService.remove(id);
  }
}
