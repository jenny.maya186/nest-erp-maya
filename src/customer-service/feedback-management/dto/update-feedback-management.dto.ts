import { PartialType } from '@nestjs/mapped-types';
import { CreateFeedbackManagementDto } from './create-feedback-management.dto';

export class UpdateFeedbackManagementDto extends PartialType(CreateFeedbackManagementDto) {
  id: number;
}
