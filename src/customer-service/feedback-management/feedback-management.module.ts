import { Module } from '@nestjs/common';
import { FeedbackManagementService } from './feedback-management.service';
import { FeedbackManagementGateway } from './feedback-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [FeedbackManagementGateway, FeedbackManagementService],
})
export class FeedbackManagementModule {}
