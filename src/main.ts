/* eslint-disable @typescript-eslint/no-unused-vars */
import * as dotenv from 'dotenv';
dotenv.config();
import 'reflect-metadata';
import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { RedisIoAdapter } from './config/redis.adaptor';
import {
  INestApplication,
  ValidationPipe,
  VersioningType,
} from '@nestjs/common';
import * as session from 'express-session';
import { EntityManager, Repository } from 'typeorm';
import { Permission } from './common/auth/entities/permission.entity';
import { Role } from './common/auth/entities/role.entity';
import { User } from './common/auth/entities/user.entity';
import {
  accessLogMiddleware,
  errorLogMiddleware,
} from './middleware/logging.middleware';
import { ExpressAdapter } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as swaggerUi from 'swagger-ui-express';
import helmet from 'helmet';
import * as express from 'express';
import {
  GATEWAY_METADATA,
  MESSAGE_METADATA,
} from '@nestjs/websockets/constants';
import { camelToSnake, camelToSpace } from './common/utils/get-client-ip.util';
import { AES } from 'crypto-js';
import { EXCLUDED_ROUTES } from './common/utils/helper.utils';

let permissionRepo: Repository<Permission>;
let roleRepo: Repository<Role>;
let userRepo: Repository<User>;

async function bootstrap() {
  const server = express();
  const app = await NestFactory.create(AppModule, new ExpressAdapter(server), {
    httpsOptions: null,
  });
  app.use(accessLogMiddleware());
  const redisIoAdapter = new RedisIoAdapter(app);
  await redisIoAdapter.connectToRedis();

  // app.enableCors();
  app.setGlobalPrefix('api');
  app.useGlobalPipes(new ValidationPipe());
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: '1',
  });

  const expressApp = app.getHttpAdapter().getInstance();
  expressApp.set('trust proxy', true);

  app.use(
    session({
      secret: `${process.env.SESSION_SECRET}`,
      resave: false,
      saveUninitialized: false,
    }),
  );

  app.useWebSocketAdapter(redisIoAdapter);

  await app.startAllMicroservices();

  if (process.env.SWAGGER_DOC === 'true') {
    const config = new DocumentBuilder()
      .addBearerAuth()
      .setTitle('Maya ERP Backend API')
      .setDescription('Maya ERP Backend API')
      .setVersion('1.0')
      .addServer(process.env.SWAGGER_SERVER)
      .addServer(process.env.SWAGGER_DEV_SERVER)
      .build();

    const document = SwaggerModule.createDocument(app, config);

    server.use(
      '/swagger',
      swaggerUi.serve,
      swaggerUi.setup(document, {
        swaggerOptions: {
          persistAuthorization: true,
        },
      }),
    );
  }

  app.use(
    helmet({
      crossOriginResourcePolicy: false,
    }),
  );
  app.use(errorLogMiddleware);
  await app.listen(process.env.PORT || 7999);

  const entityManager: EntityManager = app.get(EntityManager);
  permissionRepo = entityManager.getRepository(Permission);
  roleRepo = entityManager.getRepository(Role);
  userRepo = entityManager.getRepository(User);

  await extractAndSaveRoutes(app);
  await associatePermissionWithAdminRole();
}
bootstrap();

async function extractAndSaveRoutes(app: INestApplication) {
  const reflector = new Reflector();
  const modules = app['container'].getModules();

  for (const [moduleKey, moduleValue] of modules) {
    for (const [providerKey, providerValue] of moduleValue.providers) {
      const { instance } = providerValue;
      if (!instance) {
        continue;
      }

      const gatewayMetadata = Reflect.getMetadata(
        GATEWAY_METADATA,
        instance.constructor,
      );
      if (gatewayMetadata) {
        const eventNames = Object.getOwnPropertyNames(
          instance.constructor.prototype,
        ).filter((method) => {
          const descriptor = Object.getOwnPropertyDescriptor(
            instance.constructor.prototype,
            method,
          );
          return (
            descriptor &&
            typeof descriptor.value === 'function' &&
            Reflect.hasMetadata(MESSAGE_METADATA, descriptor.value)
          );
        });

        const gateway = instance.constructor.name;
        const gateWayName = camelToSpace(gateway)
          .replace(' ', '')
          .replaceAll(' Gateway', '');

        for (const event of eventNames) {
          const eventDescriptor = Object.getOwnPropertyDescriptor(
            instance.constructor.prototype,
            event,
          );
          if (!eventDescriptor || typeof eventDescriptor.value !== 'function') {
            continue; // Skip non-function properties
          }
          const eventName = Reflect.getMetadata(
            MESSAGE_METADATA,
            eventDescriptor.value,
          );
          if (!eventName) {
            continue; // Skip if event name metadata is missing
          }

          const eventMetadata = camelToSnake(eventName)
            .replaceAll('.', '_')
            .replaceAll('-', '_')
            .toUpperCase();

          if (EXCLUDED_ROUTES.includes(eventMetadata)) {
            continue;
          }
          let isExist = await permissionRepo.findOne({
            where: {
              name: eventMetadata,
            },
          });
          if (!isExist) {
            isExist = new Permission();
            isExist.name = `${eventMetadata}`;
            isExist.guard = gateWayName;
            isExist.type = 'WS';
            isExist.description = `Permission for WebSocket event ${eventName} from Gateway ${gateway}`;
          } else {
            isExist.name = eventMetadata;
            isExist.guard = gateWayName;
            isExist.type = 'WS';
            isExist.description = `Permission for WebSocket event ${eventName} from Gateway ${gateway}`;
          }
          await permissionRepo.save(isExist);
        }
      }
    }
  }
}

async function associatePermissionWithAdminRole() {
  const permissions: Permission[] = await permissionRepo.find();
  let role = await roleRepo.findOne({
    where: { name: 'superadmin' },
    relations: ['permissions', 'users'],
  });
  if (!role) {
    // If the admin role doesn't exist, create it and assign all permissions
    role = new Role();
    role.name = 'superadmin';
    role.description = 'Administrator';
    role.permissions = permissions;
    await roleRepo.save(role);
  } else {
    // If the admin role exists, make sure all permissions are assigned
    for (const permission of permissions) {
      if (!role.permissions.some((p) => p.id === permission.id)) {
        role.permissions.push(permission);
      }
    }
    await roleRepo.save(role);
  }
  const secret =
    process.env.ENCRYPTION_KEY ||
    'IGBAHkyfgcugvoB0gTVKxr47XjS5SDkl8n2O77j5xbFPtl91q';
  const username = process.env.SUPER_ADMIN_USERNAME || 'superadmin';
  const pwd = process.env.SUPER_ADMIN_PASSWORD || 'Admin@123';
  let user = await userRepo.findOne({
    where: { roles: { id: role.id }, username },
  });
  if (!user) {
    user = new User();
    user.name = 'Super Admin';
    user.username = username;
    user.roles = role;
    const hashPassord = AES.encrypt(pwd, secret).toString();
    user.password = hashPassord;
    await userRepo.save(user);
  }
}
