import { Module } from '@nestjs/common';
import { AccountsPayableModule } from './accounts-payable/accounts-payable.module';
import { AccountsReceivableModule } from './accounts-receivable/accounts-receivable.module';
import { ExpenseManagementModule } from './expense-management/expense-management.module';
import { PayrollModule } from './payroll/payroll.module';
import { BudgetForecastingModule } from './budget-forecasting/budget-forecasting.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [AccountsPayableModule, AccountsReceivableModule, ExpenseManagementModule, PayrollModule, BudgetForecastingModule]
})
export class FinanceModule {}
