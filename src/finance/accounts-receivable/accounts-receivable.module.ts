import { Module } from '@nestjs/common';
import { AccountsReceivableService } from './accounts-receivable.service';
import { AccountsReceivableGateway } from './accounts-receivable.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [AccountsReceivableGateway, AccountsReceivableService],
})
export class AccountsReceivableModule {}
