import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { AccountsReceivableService } from './accounts-receivable.service';
import { CreateAccountsReceivableDto } from './dto/create-accounts-receivable.dto';
import { UpdateAccountsReceivableDto } from './dto/update-accounts-receivable.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'account-receivable' })
export class AccountsReceivableGateway extends BaseGateway {
  constructor(
    private readonly accountsReceivableService: AccountsReceivableService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createAccountsReceivable')
  create(
    @MessageBody() createAccountsReceivableDto: CreateAccountsReceivableDto,
  ) {
    return this.accountsReceivableService.create(createAccountsReceivableDto);
  }

  @SubscribeMessage('findAllAccountsReceivable')
  findAll() {
    return this.accountsReceivableService.findAll();
  }

  @SubscribeMessage('findOneAccountsReceivable')
  findOne(@MessageBody() id: number) {
    return this.accountsReceivableService.findOne(id);
  }

  @SubscribeMessage('updateAccountsReceivable')
  update(
    @MessageBody() updateAccountsReceivableDto: UpdateAccountsReceivableDto,
  ) {
    return this.accountsReceivableService.update(
      updateAccountsReceivableDto.id,
      updateAccountsReceivableDto,
    );
  }

  @SubscribeMessage('removeAccountsReceivable')
  remove(@MessageBody() id: number) {
    return this.accountsReceivableService.remove(id);
  }
}
