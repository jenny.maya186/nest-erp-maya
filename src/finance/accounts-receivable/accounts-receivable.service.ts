import { Injectable } from '@nestjs/common';
import { CreateAccountsReceivableDto } from './dto/create-accounts-receivable.dto';
import { UpdateAccountsReceivableDto } from './dto/update-accounts-receivable.dto';

@Injectable()
export class AccountsReceivableService {
  create(createAccountsReceivableDto: CreateAccountsReceivableDto) {
    return 'This action adds a new accountsReceivable';
  }

  findAll() {
    return `This action returns all accountsReceivable`;
  }

  findOne(id: number) {
    return `This action returns a #${id} accountsReceivable`;
  }

  update(id: number, updateAccountsReceivableDto: UpdateAccountsReceivableDto) {
    return `This action updates a #${id} accountsReceivable`;
  }

  remove(id: number) {
    return `This action removes a #${id} accountsReceivable`;
  }
}
