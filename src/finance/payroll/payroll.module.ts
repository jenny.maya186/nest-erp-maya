import { Module } from '@nestjs/common';
import { PayrollService } from './payroll.service';
import { PayrollGateway } from './payroll.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [PayrollGateway, PayrollService],
})
export class PayrollModule {}
