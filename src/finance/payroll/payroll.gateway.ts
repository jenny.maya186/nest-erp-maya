import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { PayrollService } from './payroll.service';
import { CreatePayrollDto } from './dto/create-payroll.dto';
import { UpdatePayrollDto } from './dto/update-payroll.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'payroll' })
export class PayrollGateway extends BaseGateway {
  constructor(
    private readonly payrollService: PayrollService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createPayroll')
  create(@MessageBody() createPayrollDto: CreatePayrollDto) {
    return this.payrollService.create(createPayrollDto);
  }

  @SubscribeMessage('findAllPayroll')
  findAll() {
    return this.payrollService.findAll();
  }

  @SubscribeMessage('findOnePayroll')
  findOne(@MessageBody() id: number) {
    return this.payrollService.findOne(id);
  }

  @SubscribeMessage('updatePayroll')
  update(@MessageBody() updatePayrollDto: UpdatePayrollDto) {
    return this.payrollService.update(updatePayrollDto.id, updatePayrollDto);
  }

  @SubscribeMessage('removePayroll')
  remove(@MessageBody() id: number) {
    return this.payrollService.remove(id);
  }
}
