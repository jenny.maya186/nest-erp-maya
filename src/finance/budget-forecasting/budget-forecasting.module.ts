import { Module } from '@nestjs/common';
import { BudgetForecastingService } from './budget-forecasting.service';
import { BudgetForecastingGateway } from './budget-forecasting.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [BudgetForecastingGateway, BudgetForecastingService],
})
export class BudgetForecastingModule {}
