import { Injectable } from '@nestjs/common';
import { CreateBudgetForecastingDto } from './dto/create-budget-forecasting.dto';
import { UpdateBudgetForecastingDto } from './dto/update-budget-forecasting.dto';

@Injectable()
export class BudgetForecastingService {
  create(createBudgetForecastingDto: CreateBudgetForecastingDto) {
    return 'This action adds a new budgetForecasting';
  }

  findAll() {
    return `This action returns all budgetForecasting`;
  }

  findOne(id: number) {
    return `This action returns a #${id} budgetForecasting`;
  }

  update(id: number, updateBudgetForecastingDto: UpdateBudgetForecastingDto) {
    return `This action updates a #${id} budgetForecasting`;
  }

  remove(id: number) {
    return `This action removes a #${id} budgetForecasting`;
  }
}
