import { PartialType } from '@nestjs/mapped-types';
import { CreateBudgetForecastingDto } from './create-budget-forecasting.dto';

export class UpdateBudgetForecastingDto extends PartialType(CreateBudgetForecastingDto) {
  id: number;
}
