import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { BudgetForecastingService } from './budget-forecasting.service';
import { CreateBudgetForecastingDto } from './dto/create-budget-forecasting.dto';
import { UpdateBudgetForecastingDto } from './dto/update-budget-forecasting.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'budget-forecasting' })
export class BudgetForecastingGateway extends BaseGateway {
  constructor(
    private readonly budgetForecastingService: BudgetForecastingService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createBudgetForecasting')
  create(
    @MessageBody() createBudgetForecastingDto: CreateBudgetForecastingDto,
  ) {
    return this.budgetForecastingService.create(createBudgetForecastingDto);
  }

  @SubscribeMessage('findAllBudgetForecasting')
  findAll() {
    return this.budgetForecastingService.findAll();
  }

  @SubscribeMessage('findOneBudgetForecasting')
  findOne(@MessageBody() id: number) {
    return this.budgetForecastingService.findOne(id);
  }

  @SubscribeMessage('updateBudgetForecasting')
  update(
    @MessageBody() updateBudgetForecastingDto: UpdateBudgetForecastingDto,
  ) {
    return this.budgetForecastingService.update(
      updateBudgetForecastingDto.id,
      updateBudgetForecastingDto,
    );
  }

  @SubscribeMessage('removeBudgetForecasting')
  remove(@MessageBody() id: number) {
    return this.budgetForecastingService.remove(id);
  }
}
