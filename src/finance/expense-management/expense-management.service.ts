import { Injectable } from '@nestjs/common';
import { CreateExpenseManagementDto } from './dto/create-expense-management.dto';
import { UpdateExpenseManagementDto } from './dto/update-expense-management.dto';

@Injectable()
export class ExpenseManagementService {
  create(createExpenseManagementDto: CreateExpenseManagementDto) {
    return 'This action adds a new expenseManagement';
  }

  findAll() {
    return `This action returns all expenseManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} expenseManagement`;
  }

  update(id: number, updateExpenseManagementDto: UpdateExpenseManagementDto) {
    return `This action updates a #${id} expenseManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} expenseManagement`;
  }
}
