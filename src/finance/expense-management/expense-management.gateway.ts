import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ExpenseManagementService } from './expense-management.service';
import { CreateExpenseManagementDto } from './dto/create-expense-management.dto';
import { UpdateExpenseManagementDto } from './dto/update-expense-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'expense' })
export class ExpenseManagementGateway extends BaseGateway {
  constructor(
    private readonly expenseManagementService: ExpenseManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createExpenseManagement')
  create(
    @MessageBody() createExpenseManagementDto: CreateExpenseManagementDto,
  ) {
    return this.expenseManagementService.create(createExpenseManagementDto);
  }

  @SubscribeMessage('findAllExpenseManagement')
  findAll() {
    return this.expenseManagementService.findAll();
  }

  @SubscribeMessage('findOneExpenseManagement')
  findOne(@MessageBody() id: number) {
    return this.expenseManagementService.findOne(id);
  }

  @SubscribeMessage('updateExpenseManagement')
  update(
    @MessageBody() updateExpenseManagementDto: UpdateExpenseManagementDto,
  ) {
    return this.expenseManagementService.update(
      updateExpenseManagementDto.id,
      updateExpenseManagementDto,
    );
  }

  @SubscribeMessage('removeExpenseManagement')
  remove(@MessageBody() id: number) {
    return this.expenseManagementService.remove(id);
  }
}
