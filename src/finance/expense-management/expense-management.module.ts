import { Module } from '@nestjs/common';
import { ExpenseManagementService } from './expense-management.service';
import { ExpenseManagementGateway } from './expense-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ExpenseManagementGateway, ExpenseManagementService],
})
export class ExpenseManagementModule {}
