import { PartialType } from '@nestjs/mapped-types';
import { CreateExpenseManagementDto } from './create-expense-management.dto';

export class UpdateExpenseManagementDto extends PartialType(CreateExpenseManagementDto) {
  id: number;
}
