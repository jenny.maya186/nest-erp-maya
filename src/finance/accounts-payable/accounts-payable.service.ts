import { Injectable } from '@nestjs/common';
import { CreateAccountsPayableDto } from './dto/create-accounts-payable.dto';
import { UpdateAccountsPayableDto } from './dto/update-accounts-payable.dto';

@Injectable()
export class AccountsPayableService {
  create(createAccountsPayableDto: CreateAccountsPayableDto) {
    return 'This action adds a new accountsPayable';
  }

  findAll() {
    return `This action returns all accountsPayable`;
  }

  findOne(id: number) {
    return `This action returns a #${id} accountsPayable`;
  }

  update(id: number, updateAccountsPayableDto: UpdateAccountsPayableDto) {
    return `This action updates a #${id} accountsPayable`;
  }

  remove(id: number) {
    return `This action removes a #${id} accountsPayable`;
  }
}
