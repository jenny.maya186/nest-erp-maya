import { Module } from '@nestjs/common';
import { AccountsPayableService } from './accounts-payable.service';
import { AccountsPayableGateway } from './accounts-payable.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [AccountsPayableGateway, AccountsPayableService],
})
export class AccountsPayableModule {}
