import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { AccountsPayableService } from './accounts-payable.service';
import { CreateAccountsPayableDto } from './dto/create-accounts-payable.dto';
import { UpdateAccountsPayableDto } from './dto/update-accounts-payable.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'account-payable' })
export class AccountsPayableGateway extends BaseGateway {
  constructor(
    private readonly accountsPayableService: AccountsPayableService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createAccountsPayable')
  create(@MessageBody() createAccountsPayableDto: CreateAccountsPayableDto) {
    return this.accountsPayableService.create(createAccountsPayableDto);
  }

  @SubscribeMessage('findAllAccountsPayable')
  findAll() {
    return this.accountsPayableService.findAll();
  }

  @SubscribeMessage('findOneAccountsPayable')
  findOne(@MessageBody() id: number) {
    return this.accountsPayableService.findOne(id);
  }

  @SubscribeMessage('updateAccountsPayable')
  update(@MessageBody() updateAccountsPayableDto: UpdateAccountsPayableDto) {
    return this.accountsPayableService.update(
      updateAccountsPayableDto.id,
      updateAccountsPayableDto,
    );
  }

  @SubscribeMessage('removeAccountsPayable')
  remove(@MessageBody() id: number) {
    return this.accountsPayableService.remove(id);
  }
}
