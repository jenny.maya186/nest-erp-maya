import { Module, forwardRef } from '@nestjs/common';
import { BaseGateway } from 'src/base.gateway';
import { AuthModule } from 'src/common/auth/auth.module';
import { UserService } from 'src/common/auth/user.service';
import { JwtWebSocketGuard } from 'src/middleware/jwt-socket.guard';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@Module({
  imports: [forwardRef(() => AuthModule)],
  providers: [
    BaseGateway,
    RedisService,
    WsCustomService,
    JwtWebSocketGuard,
    UserService,
  ],
  exports: [
    BaseGateway,
    RedisService,
    WsCustomService,
    JwtWebSocketGuard,
    UserService,
  ],
})
export class BaseGatewayModule {}
