import { PartialType } from '@nestjs/mapped-types';
import { CreateBillOfMaterialsBomDto } from './create-bill-of-materials-bom.dto';

export class UpdateBillOfMaterialsBomDto extends PartialType(CreateBillOfMaterialsBomDto) {
  id: number;
}
