import { Injectable } from '@nestjs/common';
import { CreateBillOfMaterialsBomDto } from './dto/create-bill-of-materials-bom.dto';
import { UpdateBillOfMaterialsBomDto } from './dto/update-bill-of-materials-bom.dto';

@Injectable()
export class BillOfMaterialsBomService {
  create(createBillOfMaterialsBomDto: CreateBillOfMaterialsBomDto) {
    return 'This action adds a new billOfMaterialsBom';
  }

  findAll() {
    return `This action returns all billOfMaterialsBom`;
  }

  findOne(id: number) {
    return `This action returns a #${id} billOfMaterialsBom`;
  }

  update(id: number, updateBillOfMaterialsBomDto: UpdateBillOfMaterialsBomDto) {
    return `This action updates a #${id} billOfMaterialsBom`;
  }

  remove(id: number) {
    return `This action removes a #${id} billOfMaterialsBom`;
  }
}
