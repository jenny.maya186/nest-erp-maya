import { Module } from '@nestjs/common';
import { BillOfMaterialsBomService } from './bill-of-materials-bom.service';
import { BillOfMaterialsBomGateway } from './bill-of-materials-bom.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [BillOfMaterialsBomGateway, BillOfMaterialsBomService],
})
export class BillOfMaterialsBomModule {}
