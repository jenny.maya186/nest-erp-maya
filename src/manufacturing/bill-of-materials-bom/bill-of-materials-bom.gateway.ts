import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { BillOfMaterialsBomService } from './bill-of-materials-bom.service';
import { CreateBillOfMaterialsBomDto } from './dto/create-bill-of-materials-bom.dto';
import { UpdateBillOfMaterialsBomDto } from './dto/update-bill-of-materials-bom.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'bill-of-material-bom' })
export class BillOfMaterialsBomGateway extends BaseGateway {
  constructor(
    private readonly billOfMaterialsBomService: BillOfMaterialsBomService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createBillOfMaterialsBom')
  create(
    @MessageBody() createBillOfMaterialsBomDto: CreateBillOfMaterialsBomDto,
  ) {
    return this.billOfMaterialsBomService.create(createBillOfMaterialsBomDto);
  }

  @SubscribeMessage('findAllBillOfMaterialsBom')
  findAll() {
    return this.billOfMaterialsBomService.findAll();
  }

  @SubscribeMessage('findOneBillOfMaterialsBom')
  findOne(@MessageBody() id: number) {
    return this.billOfMaterialsBomService.findOne(id);
  }

  @SubscribeMessage('updateBillOfMaterialsBom')
  update(
    @MessageBody() updateBillOfMaterialsBomDto: UpdateBillOfMaterialsBomDto,
  ) {
    return this.billOfMaterialsBomService.update(
      updateBillOfMaterialsBomDto.id,
      updateBillOfMaterialsBomDto,
    );
  }

  @SubscribeMessage('removeBillOfMaterialsBom')
  remove(@MessageBody() id: number) {
    return this.billOfMaterialsBomService.remove(id);
  }
}
