import { Injectable } from '@nestjs/common';
import { CreateMaintenanceManagementServiceDto } from './dto/create-maintenance-management-service.dto';
import { UpdateMaintenanceManagementServiceDto } from './dto/update-maintenance-management-service.dto';

@Injectable()
export class MaintenanceManagementServiceService {
  create(createMaintenanceManagementServiceDto: CreateMaintenanceManagementServiceDto) {
    return 'This action adds a new maintenanceManagementService';
  }

  findAll() {
    return `This action returns all maintenanceManagementService`;
  }

  findOne(id: number) {
    return `This action returns a #${id} maintenanceManagementService`;
  }

  update(id: number, updateMaintenanceManagementServiceDto: UpdateMaintenanceManagementServiceDto) {
    return `This action updates a #${id} maintenanceManagementService`;
  }

  remove(id: number) {
    return `This action removes a #${id} maintenanceManagementService`;
  }
}
