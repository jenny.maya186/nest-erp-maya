import { Module } from '@nestjs/common';
import { MaintenanceManagementServiceService } from './maintenance-management-service.service';
import { MaintenanceManagementServiceGateway } from './maintenance-management-service.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [MaintenanceManagementServiceGateway, MaintenanceManagementServiceService],
})
export class MaintenanceManagementServiceModule {}
