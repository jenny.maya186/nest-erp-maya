import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { MaintenanceManagementServiceService } from './maintenance-management-service.service';
import { CreateMaintenanceManagementServiceDto } from './dto/create-maintenance-management-service.dto';
import { UpdateMaintenanceManagementServiceDto } from './dto/update-maintenance-management-service.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'maintenance' })
export class MaintenanceManagementServiceGateway extends BaseGateway {
  constructor(
    private readonly maintenanceManagementServiceService: MaintenanceManagementServiceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createMaintenanceManagementService')
  create(
    @MessageBody()
    createMaintenanceManagementServiceDto: CreateMaintenanceManagementServiceDto,
  ) {
    return this.maintenanceManagementServiceService.create(
      createMaintenanceManagementServiceDto,
    );
  }

  @SubscribeMessage('findAllMaintenanceManagementService')
  findAll() {
    return this.maintenanceManagementServiceService.findAll();
  }

  @SubscribeMessage('findOneMaintenanceManagementService')
  findOne(@MessageBody() id: number) {
    return this.maintenanceManagementServiceService.findOne(id);
  }

  @SubscribeMessage('updateMaintenanceManagementService')
  update(
    @MessageBody()
    updateMaintenanceManagementServiceDto: UpdateMaintenanceManagementServiceDto,
  ) {
    return this.maintenanceManagementServiceService.update(
      updateMaintenanceManagementServiceDto.id,
      updateMaintenanceManagementServiceDto,
    );
  }

  @SubscribeMessage('removeMaintenanceManagementService')
  remove(@MessageBody() id: number) {
    return this.maintenanceManagementServiceService.remove(id);
  }
}
