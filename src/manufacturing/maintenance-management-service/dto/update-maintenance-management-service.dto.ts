import { PartialType } from '@nestjs/mapped-types';
import { CreateMaintenanceManagementServiceDto } from './create-maintenance-management-service.dto';

export class UpdateMaintenanceManagementServiceDto extends PartialType(CreateMaintenanceManagementServiceDto) {
  id: number;
}
