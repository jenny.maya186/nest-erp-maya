import { Injectable } from '@nestjs/common';
import { CreateProductionPlanningServiceDto } from './dto/create-production-planning-service.dto';
import { UpdateProductionPlanningServiceDto } from './dto/update-production-planning-service.dto';

@Injectable()
export class ProductionPlanningServiceService {
  create(createProductionPlanningServiceDto: CreateProductionPlanningServiceDto) {
    return 'This action adds a new productionPlanningService';
  }

  findAll() {
    return `This action returns all productionPlanningService`;
  }

  findOne(id: number) {
    return `This action returns a #${id} productionPlanningService`;
  }

  update(id: number, updateProductionPlanningServiceDto: UpdateProductionPlanningServiceDto) {
    return `This action updates a #${id} productionPlanningService`;
  }

  remove(id: number) {
    return `This action removes a #${id} productionPlanningService`;
  }
}
