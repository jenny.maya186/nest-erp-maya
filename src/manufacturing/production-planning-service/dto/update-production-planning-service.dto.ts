import { PartialType } from '@nestjs/mapped-types';
import { CreateProductionPlanningServiceDto } from './create-production-planning-service.dto';

export class UpdateProductionPlanningServiceDto extends PartialType(CreateProductionPlanningServiceDto) {
  id: number;
}
