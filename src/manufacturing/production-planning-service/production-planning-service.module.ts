import { Module } from '@nestjs/common';
import { ProductionPlanningServiceService } from './production-planning-service.service';
import { ProductionPlanningServiceGateway } from './production-planning-service.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ProductionPlanningServiceGateway, ProductionPlanningServiceService],
})
export class ProductionPlanningServiceModule {}
