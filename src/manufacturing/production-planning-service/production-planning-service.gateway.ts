import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ProductionPlanningServiceService } from './production-planning-service.service';
import { CreateProductionPlanningServiceDto } from './dto/create-production-planning-service.dto';
import { UpdateProductionPlanningServiceDto } from './dto/update-production-planning-service.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'production-planning' })
export class ProductionPlanningServiceGateway extends BaseGateway {
  constructor(
    private readonly productionPlanningServiceService: ProductionPlanningServiceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createProductionPlanningService')
  create(
    @MessageBody()
    createProductionPlanningServiceDto: CreateProductionPlanningServiceDto,
  ) {
    return this.productionPlanningServiceService.create(
      createProductionPlanningServiceDto,
    );
  }

  @SubscribeMessage('findAllProductionPlanningService')
  findAll() {
    return this.productionPlanningServiceService.findAll();
  }

  @SubscribeMessage('findOneProductionPlanningService')
  findOne(@MessageBody() id: number) {
    return this.productionPlanningServiceService.findOne(id);
  }

  @SubscribeMessage('updateProductionPlanningService')
  update(
    @MessageBody()
    updateProductionPlanningServiceDto: UpdateProductionPlanningServiceDto,
  ) {
    return this.productionPlanningServiceService.update(
      updateProductionPlanningServiceDto.id,
      updateProductionPlanningServiceDto,
    );
  }

  @SubscribeMessage('removeProductionPlanningService')
  remove(@MessageBody() id: number) {
    return this.productionPlanningServiceService.remove(id);
  }
}
