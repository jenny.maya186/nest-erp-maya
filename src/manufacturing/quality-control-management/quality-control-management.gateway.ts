import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { QualityControlManagementService } from './quality-control-management.service';
import { CreateQualityControlManagementDto } from './dto/create-quality-control-management.dto';
import { UpdateQualityControlManagementDto } from './dto/update-quality-control-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'quality-control' })
export class QualityControlManagementGateway extends BaseGateway {
  constructor(
    private readonly qualityControlManagementService: QualityControlManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createQualityControlManagement')
  create(
    @MessageBody()
    createQualityControlManagementDto: CreateQualityControlManagementDto,
  ) {
    return this.qualityControlManagementService.create(
      createQualityControlManagementDto,
    );
  }

  @SubscribeMessage('findAllQualityControlManagement')
  findAll() {
    return this.qualityControlManagementService.findAll();
  }

  @SubscribeMessage('findOneQualityControlManagement')
  findOne(@MessageBody() id: number) {
    return this.qualityControlManagementService.findOne(id);
  }

  @SubscribeMessage('updateQualityControlManagement')
  update(
    @MessageBody()
    updateQualityControlManagementDto: UpdateQualityControlManagementDto,
  ) {
    return this.qualityControlManagementService.update(
      updateQualityControlManagementDto.id,
      updateQualityControlManagementDto,
    );
  }

  @SubscribeMessage('removeQualityControlManagement')
  remove(@MessageBody() id: number) {
    return this.qualityControlManagementService.remove(id);
  }
}
