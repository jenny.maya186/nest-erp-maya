import { PartialType } from '@nestjs/mapped-types';
import { CreateQualityControlManagementDto } from './create-quality-control-management.dto';

export class UpdateQualityControlManagementDto extends PartialType(CreateQualityControlManagementDto) {
  id: number;
}
