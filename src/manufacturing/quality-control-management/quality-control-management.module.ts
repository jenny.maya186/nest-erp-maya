import { Module } from '@nestjs/common';
import { QualityControlManagementService } from './quality-control-management.service';
import { QualityControlManagementGateway } from './quality-control-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [QualityControlManagementGateway, QualityControlManagementService],
})
export class QualityControlManagementModule {}
