import { Injectable } from '@nestjs/common';
import { CreateQualityControlManagementDto } from './dto/create-quality-control-management.dto';
import { UpdateQualityControlManagementDto } from './dto/update-quality-control-management.dto';

@Injectable()
export class QualityControlManagementService {
  create(createQualityControlManagementDto: CreateQualityControlManagementDto) {
    return 'This action adds a new qualityControlManagement';
  }

  findAll() {
    return `This action returns all qualityControlManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} qualityControlManagement`;
  }

  update(id: number, updateQualityControlManagementDto: UpdateQualityControlManagementDto) {
    return `This action updates a #${id} qualityControlManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} qualityControlManagement`;
  }
}
