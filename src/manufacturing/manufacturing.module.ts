import { Module } from '@nestjs/common';
import { ProductionPlanningServiceModule } from './production-planning-service/production-planning-service.module';
import { BillOfMaterialsBomModule } from './bill-of-materials-bom/bill-of-materials-bom.module';
import { QualityControlManagementModule } from './quality-control-management/quality-control-management.module';
import { MaintenanceManagementServiceModule } from './maintenance-management-service/maintenance-management-service.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [
    ProductionPlanningServiceModule,
    BillOfMaterialsBomModule,
    QualityControlManagementModule,
    MaintenanceManagementServiceModule,
  ],
})
export class ManufacturingModule {}
