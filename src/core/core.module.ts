import { Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { GeneralLadgerModule } from './general-ladger/general-ladger.module';
import { ReportsModule } from './reports/reports.module';
import { AnalyticsModule } from './analytics/analytics.module';
import { DataWarehouseModule } from './data-warehouse/data-warehouse.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [ConfigModule, GeneralLadgerModule, ReportsModule, AnalyticsModule, DataWarehouseModule]
})
export class CoreModule {}
