import { Module } from '@nestjs/common';
import { DataWarehouseService } from './data-warehouse.service';
import { DataWarehouseGateway } from './data-warehouse.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [DataWarehouseGateway, DataWarehouseService],
})
export class DataWarehouseModule {}
