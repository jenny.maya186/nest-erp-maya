import { Injectable } from '@nestjs/common';
import { CreateDataWarehouseDto } from './dto/create-data-warehouse.dto';
import { UpdateDataWarehouseDto } from './dto/update-data-warehouse.dto';

@Injectable()
export class DataWarehouseService {
  create(createDataWarehouseDto: CreateDataWarehouseDto) {
    return 'This action adds a new dataWarehouse';
  }

  findAll() {
    return `This action returns all dataWarehouse`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dataWarehouse`;
  }

  update(id: number, updateDataWarehouseDto: UpdateDataWarehouseDto) {
    return `This action updates a #${id} dataWarehouse`;
  }

  remove(id: number) {
    return `This action removes a #${id} dataWarehouse`;
  }
}
