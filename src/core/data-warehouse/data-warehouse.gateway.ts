import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { DataWarehouseService } from './data-warehouse.service';
import { CreateDataWarehouseDto } from './dto/create-data-warehouse.dto';
import { UpdateDataWarehouseDto } from './dto/update-data-warehouse.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'data-warehouse' })
export class DataWarehouseGateway extends BaseGateway {
  constructor(
    private readonly dataWarehouseService: DataWarehouseService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createDataWarehouse')
  create(@MessageBody() createDataWarehouseDto: CreateDataWarehouseDto) {
    return this.dataWarehouseService.create(createDataWarehouseDto);
  }

  @SubscribeMessage('findAllDataWarehouse')
  findAll() {
    return this.dataWarehouseService.findAll();
  }

  @SubscribeMessage('findOneDataWarehouse')
  findOne(@MessageBody() id: number) {
    return this.dataWarehouseService.findOne(id);
  }

  @SubscribeMessage('updateDataWarehouse')
  update(@MessageBody() updateDataWarehouseDto: UpdateDataWarehouseDto) {
    return this.dataWarehouseService.update(
      updateDataWarehouseDto.id,
      updateDataWarehouseDto,
    );
  }

  @SubscribeMessage('removeDataWarehouse')
  remove(@MessageBody() id: number) {
    return this.dataWarehouseService.remove(id);
  }
}
