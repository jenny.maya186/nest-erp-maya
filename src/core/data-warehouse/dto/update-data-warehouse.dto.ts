import { PartialType } from '@nestjs/mapped-types';
import { CreateDataWarehouseDto } from './create-data-warehouse.dto';

export class UpdateDataWarehouseDto extends PartialType(CreateDataWarehouseDto) {
  id: number;
}
