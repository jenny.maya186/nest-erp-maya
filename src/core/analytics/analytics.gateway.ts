import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { AnalyticsService } from './analytics.service';
import { CreateAnalyticsDto } from './dto/create-analytics.dto';
import { UpdateAnalyticsDto } from './dto/update-analytics.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'analytics' })
export class AnalyticsGateway extends BaseGateway {
  constructor(
    private readonly analyticsService: AnalyticsService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createAnalytics')
  create(@MessageBody() createAnalyticsDto: CreateAnalyticsDto) {
    return this.analyticsService.create(createAnalyticsDto);
  }

  @SubscribeMessage('findAllAnalytics')
  findAll() {
    return this.analyticsService.findAll();
  }

  @SubscribeMessage('findOneAnalytics')
  findOne(@MessageBody() id: number) {
    return this.analyticsService.findOne(id);
  }

  @SubscribeMessage('updateAnalytics')
  update(@MessageBody() updateAnalyticsDto: UpdateAnalyticsDto) {
    return this.analyticsService.update(
      updateAnalyticsDto.id,
      updateAnalyticsDto,
    );
  }

  @SubscribeMessage('removeAnalytics')
  remove(@MessageBody() id: number) {
    return this.analyticsService.remove(id);
  }
}
