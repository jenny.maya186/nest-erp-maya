import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ConfigService } from './config.service';
import { CreateConfigDto } from './dto/create-config.dto';
import { UpdateConfigDto } from './dto/update-config.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'config' })
export class ConfigGateway extends BaseGateway {
  constructor(
    private readonly configService: ConfigService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createConfig')
  create(@MessageBody() createConfigDto: CreateConfigDto) {
    return this.configService.create(createConfigDto);
  }

  @SubscribeMessage('findAllConfig')
  findAll() {
    return this.configService.findAll();
  }

  @SubscribeMessage('findOneConfig')
  findOne(@MessageBody() id: number) {
    return this.configService.findOne(id);
  }

  @SubscribeMessage('updateConfig')
  update(@MessageBody() updateConfigDto: UpdateConfigDto) {
    return this.configService.update(updateConfigDto.id, updateConfigDto);
  }

  @SubscribeMessage('removeConfig')
  remove(@MessageBody() id: number) {
    return this.configService.remove(id);
  }
}
