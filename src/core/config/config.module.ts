import { Module } from '@nestjs/common';
import { ConfigService } from './config.service';
import { ConfigGateway } from './config.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ConfigGateway, ConfigService],
})
export class ConfigModule {}
