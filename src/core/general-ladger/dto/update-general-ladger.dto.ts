import { PartialType } from '@nestjs/mapped-types';
import { CreateGeneralLadgerDto } from './create-general-ladger.dto';

export class UpdateGeneralLadgerDto extends PartialType(CreateGeneralLadgerDto) {
  id: number;
}
