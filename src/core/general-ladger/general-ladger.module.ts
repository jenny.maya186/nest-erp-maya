import { Module } from '@nestjs/common';
import { GeneralLadgerService } from './general-ladger.service';
import { GeneralLadgerGateway } from './general-ladger.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [GeneralLadgerGateway, GeneralLadgerService],
})
export class GeneralLadgerModule {}
