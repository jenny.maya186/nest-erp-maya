import { Injectable } from '@nestjs/common';
import { CreateGeneralLadgerDto } from './dto/create-general-ladger.dto';
import { UpdateGeneralLadgerDto } from './dto/update-general-ladger.dto';

@Injectable()
export class GeneralLadgerService {
  create(createGeneralLadgerDto: CreateGeneralLadgerDto) {
    return 'This action adds a new generalLadger';
  }

  findAll() {
    return `This action returns all generalLadger`;
  }

  findOne(id: number) {
    return `This action returns a #${id} generalLadger`;
  }

  update(id: number, updateGeneralLadgerDto: UpdateGeneralLadgerDto) {
    return `This action updates a #${id} generalLadger`;
  }

  remove(id: number) {
    return `This action removes a #${id} generalLadger`;
  }
}
