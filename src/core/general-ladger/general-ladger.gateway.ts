import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { GeneralLadgerService } from './general-ladger.service';
import { CreateGeneralLadgerDto } from './dto/create-general-ladger.dto';
import { UpdateGeneralLadgerDto } from './dto/update-general-ladger.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'general-ledger' })
export class GeneralLadgerGateway extends BaseGateway {
  constructor(
    private readonly generalLadgerService: GeneralLadgerService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createGeneralLadger')
  create(@MessageBody() createGeneralLadgerDto: CreateGeneralLadgerDto) {
    return this.generalLadgerService.create(createGeneralLadgerDto);
  }

  @SubscribeMessage('findAllGeneralLadger')
  findAll() {
    return this.generalLadgerService.findAll();
  }

  @SubscribeMessage('findOneGeneralLadger')
  findOne(@MessageBody() id: number) {
    return this.generalLadgerService.findOne(id);
  }

  @SubscribeMessage('updateGeneralLadger')
  update(@MessageBody() updateGeneralLadgerDto: UpdateGeneralLadgerDto) {
    return this.generalLadgerService.update(
      updateGeneralLadgerDto.id,
      updateGeneralLadgerDto,
    );
  }

  @SubscribeMessage('removeGeneralLadger')
  remove(@MessageBody() id: number) {
    return this.generalLadgerService.remove(id);
  }
}
