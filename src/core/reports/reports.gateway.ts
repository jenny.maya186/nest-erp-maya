import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ReportsService } from './reports.service';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'reports' })
export class ReportsGateway extends BaseGateway {
  constructor(
    private readonly reportsService: ReportsService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createReport')
  create(@MessageBody() createReportDto: CreateReportDto) {
    return this.reportsService.create(createReportDto);
  }

  @SubscribeMessage('findAllReports')
  findAll() {
    return this.reportsService.findAll();
  }

  @SubscribeMessage('findOneReport')
  findOne(@MessageBody() id: number) {
    return this.reportsService.findOne(id);
  }

  @SubscribeMessage('updateReport')
  update(@MessageBody() updateReportDto: UpdateReportDto) {
    return this.reportsService.update(updateReportDto.id, updateReportDto);
  }

  @SubscribeMessage('removeReport')
  remove(@MessageBody() id: number) {
    return this.reportsService.remove(id);
  }
}
