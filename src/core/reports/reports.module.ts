import { Module } from '@nestjs/common';
import { ReportsService } from './reports.service';
import { ReportsGateway } from './reports.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ReportsGateway, ReportsService],
})
export class ReportsModule {}
