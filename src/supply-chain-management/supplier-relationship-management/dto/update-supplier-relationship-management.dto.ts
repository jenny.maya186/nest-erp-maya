import { PartialType } from '@nestjs/mapped-types';
import { CreateSupplierRelationshipManagementDto } from './create-supplier-relationship-management.dto';

export class UpdateSupplierRelationshipManagementDto extends PartialType(CreateSupplierRelationshipManagementDto) {
  id: number;
}
