import { Module } from '@nestjs/common';
import { SupplierRelationshipManagementService } from './supplier-relationship-management.service';
import { SupplierRelationshipManagementGateway } from './supplier-relationship-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [SupplierRelationshipManagementGateway, SupplierRelationshipManagementService],
})
export class SupplierRelationshipManagementModule {}
