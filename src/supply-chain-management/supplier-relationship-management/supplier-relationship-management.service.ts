import { Injectable } from '@nestjs/common';
import { CreateSupplierRelationshipManagementDto } from './dto/create-supplier-relationship-management.dto';
import { UpdateSupplierRelationshipManagementDto } from './dto/update-supplier-relationship-management.dto';

@Injectable()
export class SupplierRelationshipManagementService {
  create(createSupplierRelationshipManagementDto: CreateSupplierRelationshipManagementDto) {
    return 'This action adds a new supplierRelationshipManagement';
  }

  findAll() {
    return `This action returns all supplierRelationshipManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} supplierRelationshipManagement`;
  }

  update(id: number, updateSupplierRelationshipManagementDto: UpdateSupplierRelationshipManagementDto) {
    return `This action updates a #${id} supplierRelationshipManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} supplierRelationshipManagement`;
  }
}
