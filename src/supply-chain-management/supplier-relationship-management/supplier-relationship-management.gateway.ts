import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { SupplierRelationshipManagementService } from './supplier-relationship-management.service';
import { CreateSupplierRelationshipManagementDto } from './dto/create-supplier-relationship-management.dto';
import { UpdateSupplierRelationshipManagementDto } from './dto/update-supplier-relationship-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'supplier-relationship-management' })
export class SupplierRelationshipManagementGateway extends BaseGateway {
  constructor(
    private readonly supplierRelationshipManagementService: SupplierRelationshipManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createSupplierRelationshipManagement')
  create(
    @MessageBody()
    createSupplierRelationshipManagementDto: CreateSupplierRelationshipManagementDto,
  ) {
    return this.supplierRelationshipManagementService.create(
      createSupplierRelationshipManagementDto,
    );
  }

  @SubscribeMessage('findAllSupplierRelationshipManagement')
  findAll() {
    return this.supplierRelationshipManagementService.findAll();
  }

  @SubscribeMessage('findOneSupplierRelationshipManagement')
  findOne(@MessageBody() id: number) {
    return this.supplierRelationshipManagementService.findOne(id);
  }

  @SubscribeMessage('updateSupplierRelationshipManagement')
  update(
    @MessageBody()
    updateSupplierRelationshipManagementDto: UpdateSupplierRelationshipManagementDto,
  ) {
    return this.supplierRelationshipManagementService.update(
      updateSupplierRelationshipManagementDto.id,
      updateSupplierRelationshipManagementDto,
    );
  }

  @SubscribeMessage('removeSupplierRelationshipManagement')
  remove(@MessageBody() id: number) {
    return this.supplierRelationshipManagementService.remove(id);
  }
}
