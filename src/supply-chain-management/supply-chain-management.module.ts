import { Module } from '@nestjs/common';
import { InventoryManagementModule } from './inventory-management/inventory-management.module';
import { ProcurementManagementModule } from './procurement-management/procurement-management.module';
import { ShippingLogisticsManagementModule } from './shipping-logistics-management/shipping-logistics-management.module';
import { SupplierRelationshipManagementModule } from './supplier-relationship-management/supplier-relationship-management.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [InventoryManagementModule, ProcurementManagementModule, ShippingLogisticsManagementModule, SupplierRelationshipManagementModule]
})
export class SupplyChainManagementModule {}
