import { Injectable } from '@nestjs/common';
import { CreateProcurementManagementDto } from './dto/create-procurement-management.dto';
import { UpdateProcurementManagementDto } from './dto/update-procurement-management.dto';

@Injectable()
export class ProcurementManagementService {
  create(createProcurementManagementDto: CreateProcurementManagementDto) {
    return 'This action adds a new procurementManagement';
  }

  findAll() {
    return `This action returns all procurementManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} procurementManagement`;
  }

  update(id: number, updateProcurementManagementDto: UpdateProcurementManagementDto) {
    return `This action updates a #${id} procurementManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} procurementManagement`;
  }
}
