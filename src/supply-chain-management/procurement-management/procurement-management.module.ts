import { Module } from '@nestjs/common';
import { ProcurementManagementService } from './procurement-management.service';
import { ProcurementManagementGateway } from './procurement-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ProcurementManagementGateway, ProcurementManagementService],
})
export class ProcurementManagementModule {}
