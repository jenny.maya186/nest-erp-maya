import { PartialType } from '@nestjs/mapped-types';
import { CreateProcurementManagementDto } from './create-procurement-management.dto';

export class UpdateProcurementManagementDto extends PartialType(CreateProcurementManagementDto) {
  id: number;
}
