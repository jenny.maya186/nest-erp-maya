import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ProcurementManagementService } from './procurement-management.service';
import { CreateProcurementManagementDto } from './dto/create-procurement-management.dto';
import { UpdateProcurementManagementDto } from './dto/update-procurement-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'procurement' })
export class ProcurementManagementGateway extends BaseGateway {
  constructor(
    private readonly procurementManagementService: ProcurementManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createProcurementManagement')
  create(
    @MessageBody()
    createProcurementManagementDto: CreateProcurementManagementDto,
  ) {
    return this.procurementManagementService.create(
      createProcurementManagementDto,
    );
  }

  @SubscribeMessage('findAllProcurementManagement')
  findAll() {
    return this.procurementManagementService.findAll();
  }

  @SubscribeMessage('findOneProcurementManagement')
  findOne(@MessageBody() id: number) {
    return this.procurementManagementService.findOne(id);
  }

  @SubscribeMessage('updateProcurementManagement')
  update(
    @MessageBody()
    updateProcurementManagementDto: UpdateProcurementManagementDto,
  ) {
    return this.procurementManagementService.update(
      updateProcurementManagementDto.id,
      updateProcurementManagementDto,
    );
  }

  @SubscribeMessage('removeProcurementManagement')
  remove(@MessageBody() id: number) {
    return this.procurementManagementService.remove(id);
  }
}
