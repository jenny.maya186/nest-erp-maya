import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { InventoryManagementService } from './inventory-management.service';
import { CreateInventoryManagementDto } from './dto/create-inventory-management.dto';
import { UpdateInventoryManagementDto } from './dto/update-inventory-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'inventory' })
export class InventoryManagementGateway extends BaseGateway {
  constructor(
    private readonly inventoryManagementService: InventoryManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createInventoryManagement')
  create(
    @MessageBody() createInventoryManagementDto: CreateInventoryManagementDto,
  ) {
    return this.inventoryManagementService.create(createInventoryManagementDto);
  }

  @SubscribeMessage('findAllInventoryManagement')
  findAll() {
    return this.inventoryManagementService.findAll();
  }

  @SubscribeMessage('findOneInventoryManagement')
  findOne(@MessageBody() id: number) {
    return this.inventoryManagementService.findOne(id);
  }

  @SubscribeMessage('updateInventoryManagement')
  update(
    @MessageBody() updateInventoryManagementDto: UpdateInventoryManagementDto,
  ) {
    return this.inventoryManagementService.update(
      updateInventoryManagementDto.id,
      updateInventoryManagementDto,
    );
  }

  @SubscribeMessage('removeInventoryManagement')
  remove(@MessageBody() id: number) {
    return this.inventoryManagementService.remove(id);
  }
}
