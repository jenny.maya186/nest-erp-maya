import { Module } from '@nestjs/common';
import { InventoryManagementService } from './inventory-management.service';
import { InventoryManagementGateway } from './inventory-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [InventoryManagementGateway, InventoryManagementService],
})
export class InventoryManagementModule {}
