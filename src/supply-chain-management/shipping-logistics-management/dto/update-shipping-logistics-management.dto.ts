import { PartialType } from '@nestjs/mapped-types';
import { CreateShippingLogisticsManagementDto } from './create-shipping-logistics-management.dto';

export class UpdateShippingLogisticsManagementDto extends PartialType(CreateShippingLogisticsManagementDto) {
  id: number;
}
