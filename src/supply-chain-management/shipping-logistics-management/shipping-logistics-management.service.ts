import { Injectable } from '@nestjs/common';
import { CreateShippingLogisticsManagementDto } from './dto/create-shipping-logistics-management.dto';
import { UpdateShippingLogisticsManagementDto } from './dto/update-shipping-logistics-management.dto';

@Injectable()
export class ShippingLogisticsManagementService {
  create(createShippingLogisticsManagementDto: CreateShippingLogisticsManagementDto) {
    return 'This action adds a new shippingLogisticsManagement';
  }

  findAll() {
    return `This action returns all shippingLogisticsManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} shippingLogisticsManagement`;
  }

  update(id: number, updateShippingLogisticsManagementDto: UpdateShippingLogisticsManagementDto) {
    return `This action updates a #${id} shippingLogisticsManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} shippingLogisticsManagement`;
  }
}
