import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ShippingLogisticsManagementService } from './shipping-logistics-management.service';
import { CreateShippingLogisticsManagementDto } from './dto/create-shipping-logistics-management.dto';
import { UpdateShippingLogisticsManagementDto } from './dto/update-shipping-logistics-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'shipping-logistics' })
export class ShippingLogisticsManagementGateway extends BaseGateway {
  constructor(
    private readonly shippingLogisticsManagementService: ShippingLogisticsManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createShippingLogisticsManagement')
  create(
    @MessageBody()
    createShippingLogisticsManagementDto: CreateShippingLogisticsManagementDto,
  ) {
    return this.shippingLogisticsManagementService.create(
      createShippingLogisticsManagementDto,
    );
  }

  @SubscribeMessage('findAllShippingLogisticsManagement')
  findAll() {
    return this.shippingLogisticsManagementService.findAll();
  }

  @SubscribeMessage('findOneShippingLogisticsManagement')
  findOne(@MessageBody() id: number) {
    return this.shippingLogisticsManagementService.findOne(id);
  }

  @SubscribeMessage('updateShippingLogisticsManagement')
  update(
    @MessageBody()
    updateShippingLogisticsManagementDto: UpdateShippingLogisticsManagementDto,
  ) {
    return this.shippingLogisticsManagementService.update(
      updateShippingLogisticsManagementDto.id,
      updateShippingLogisticsManagementDto,
    );
  }

  @SubscribeMessage('removeShippingLogisticsManagement')
  remove(@MessageBody() id: number) {
    return this.shippingLogisticsManagementService.remove(id);
  }
}
