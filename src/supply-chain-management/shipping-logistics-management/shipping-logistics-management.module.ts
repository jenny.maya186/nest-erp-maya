import { Module } from '@nestjs/common';
import { ShippingLogisticsManagementService } from './shipping-logistics-management.service';
import { ShippingLogisticsManagementGateway } from './shipping-logistics-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ShippingLogisticsManagementGateway, ShippingLogisticsManagementService],
})
export class ShippingLogisticsManagementModule {}
