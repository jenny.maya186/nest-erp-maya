// socket.service.ts

import { Injectable } from '@nestjs/common';
import { Server } from 'socket.io';

@Injectable()
export class WsCustomService {
  private server: Server;

  setServer(server: Server): void {
    this.server = server;
    this.server.setMaxListeners(100);
  }

  broadcastToClient(clientId: string, event: string, message: any): void {
    this.server.to(clientId).emit(event, message);
  }
}
