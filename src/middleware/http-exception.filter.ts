import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  Injectable,
} from '@nestjs/common';
import { Response } from 'express';
import logger from 'src/common/utils/logger';
import { ApiResponse } from 'src/common/utils/response.util';
import { Telegram } from 'telegraf';
import { bold, code, join, underline } from 'telegraf/format';

@Catch(HttpException)
@Injectable()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const req = ctx.getRequest<any>();
    const status = exception.getStatus();

    logger.error(exception.getResponse());
    if (process.env.TG_LOG === 'true')
      try {
        const telegram = new Telegram(process.env.TG_BOT_TOKEN as string);
        const message = join([
          bold(underline(exception.name)),
          code(exception.stack?.replaceAll(exception.name, '')),
        ]);
        telegram.sendMessage(process.env.TG_CHAT_ID, message).catch((e) => {
          console.log('ee::::>>>>' + e.response);
        });
      } catch (e) {
        console.log('ee::::>>>>' + e);
      }

    const lang = req.headers?.lang?.toLowerCase() || 'zh';

    let errorResponse: any; // Define a variable to store the error response

    if (typeof exception.getResponse() === 'object') {
      errorResponse = exception.getResponse();
    } else {
      // If it's a string, create an object with an 'error' property
      errorResponse = { error: exception.getResponse() as string };
    }
    // console.log(typeof errorResponse.message);
    console.log(errorResponse);

    const res = ApiResponse(null, status, errorResponse);
    res['causeBy'] = errorResponse.cause;
    response.status(status).json(res);
  }
}
