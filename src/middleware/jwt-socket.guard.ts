import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { WsException } from '@nestjs/websockets';
import { AES, enc } from 'crypto-js';
import { Socket } from 'socket.io';
import { UserService } from 'src/common/auth/user.service';
import { camelToSnake } from 'src/common/utils/get-client-ip.util';
import {
  CHECK_LOGIN_ROUTES,
  EXCLUDED_ROUTES,
} from 'src/common/utils/helper.utils';

@Injectable()
export class JwtWebSocketGuard implements CanActivate {
  constructor(
    @Inject(forwardRef(() => JwtService))
    private readonly jwtService: JwtService,
    @Inject(forwardRef(() => UserService))
    private readonly usersService: UserService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    const args = context.getArgs();
    let eventName = args[args.length - 1];

    try {
      if (eventName) {
        eventName = camelToSnake(eventName)
          .replaceAll('.', '_')
          .replaceAll('-', '_')
          .toUpperCase();
      }

      if (EXCLUDED_ROUTES.includes(eventName)) {
        return true;
      }

      const token = client.handshake?.headers?.token;
      console.log('JwtWebSocketGuard called ', eventName);

      if (!token) {
        throw new WsException({
          statusCode: 401,
          message: 'TOKEN_REQUIRED',
        });
      }

      let tokenString: string;
      if (Array.isArray(token)) {
        tokenString = token[0]; // Use the first token if multiple are provided
      } else {
        tokenString = token;
      }

      try {
        const decryptedToken = AES.decrypt(
          tokenString,
          process.env.ENCRYPTION_KEY_TOKEN,
        ).toString(enc.Utf8);

        if (!decryptedToken) {
          throw new WsException({
            statusCode: 401,
            message: 'INVALID_TOKEN',
          });
        }

        const decoded = this.jwtService.verify(decryptedToken, {
          secret: process.env.JWT_SECRET,
        });

        const user = await this.usersService.findById(decoded.userId);
        if (!user) {
          throw new WsException({
            statusCode: 404,
            message: 'INVALID_TOKEN_USER',
          });
        }

        if (CHECK_LOGIN_ROUTES.includes(eventName)) {
          return true;
        }

        const canAccess = await this.usersService.canAccess(
          decoded.userId,
          eventName,
        );

        if (!canAccess) {
          throw new UnauthorizedException({
            statusCode: 401,
            message: 'UNAUTHORIZED_ACCESS',
          });
        }
        return true;
      } catch (error) {
        throw new WsException({
          statusCode: 401,
          message: error.message,
        });
      }
    } catch (error) {
      client.emit('error', error);
      return false;
    }
  }
}
