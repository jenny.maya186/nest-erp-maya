import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  Injectable,
} from '@nestjs/common';
import { Response } from 'express';
import logger from 'src/common/utils/logger';
import { ApiResponse } from 'src/common/utils/response.util';

@Catch()
@Injectable()
export class DbExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const req = ctx.getRequest<any>();
    logger.error(exception);

    req.isError = true;

    const res = ApiResponse(null, 400, exception.message);
    res['causeBy'] = exception;
    response.status(400).json(res);
  }
}
