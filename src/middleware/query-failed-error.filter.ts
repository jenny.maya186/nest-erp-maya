import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { QueryFailedError } from 'typeorm';
import { Telegram } from 'telegraf';
import { bold, code, join, underline } from 'telegraf/format';

@Catch(QueryFailedError)
export class QueryFailedErrorFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: QueryFailedError, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;
    const ctx = host.switchToHttp();

    if (process.env.TG_LOG === 'true')
      try {
        const telegram = new Telegram(process.env.TG_BOT_TOKEN as string);
        const message = join([
          bold(underline(exception.name)),
          code(exception.stack?.replaceAll(exception.name, '')),
        ]);
        telegram.sendMessage(process.env.TG_CHAT_ID, message).catch((e) => {
          console.log('ee::::>>>>' + e.response);
        });
      } catch (e) {
        console.log('ee::::>>>>' + e);
      }

    const responseBody = {
      data: null,
      statusCode: HttpStatus.BAD_REQUEST,
      message: exception.driverError.message,
    };
    switch (exception.driverError.message) {
      case 'ER_DUP_ENTRY':
        const match = /Duplicate entry '(.*?)' for key/.exec(
          exception.driverError.message,
        );
        const duplicatedValue = match ? match[1] : null;
        responseBody['error'] = ['ER_DUP_ENTRY', duplicatedValue];
        break;
      case 'ER_NO_REFERENCED_ROW_2':
        responseBody['error'] = ['ER_NO_REFERENCED_ROW_2'];
        break;
      default: {
        responseBody['error'] = [exception.driverError.message];
      }
    }

    httpAdapter.reply(ctx.getResponse(), responseBody, HttpStatus.BAD_REQUEST);
  }
}
