import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
  BadRequestException,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

@Catch(Error)
export class ErrorFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: Error, host: ArgumentsHost): void {
    console.error(exception.name, '-', exception.message);

    const { httpAdapter } = this.httpAdapterHost;
    if (!httpAdapter) {
      // Handle the case where httpAdapter is not available
      throw exception;
    }

    const ctx = host.switchToHttp();
    let statusCode = HttpStatus.INTERNAL_SERVER_ERROR;

    if (exception instanceof BadRequestException) {
      // If it's a BadRequestException, use HttpStatus.BAD_REQUEST
      statusCode = HttpStatus.BAD_REQUEST;
    }

    const responseBody = {
      data: null,
      statusCode,
      message: exception.message,
      cause: exception.stack,
    };

    httpAdapter.reply(ctx.getResponse(), responseBody, statusCode);
  }
}
