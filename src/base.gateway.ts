/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { Global, UseGuards } from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { WsCustomService } from './ws.service';
import { RedisService } from './redis.service';
import { GetJsonFromString } from './common/utils/helper.utils';
import { JwtWebSocketGuard } from './middleware/jwt-socket.guard';

@Global()
@WebSocketGateway({ cors: true })
@UseGuards(JwtWebSocketGuard)
export class BaseGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private readonly redisService: RedisService,
    private readonly wsService: WsCustomService,
  ) {}

  @WebSocketServer()
  server: Server;

  afterInit(server: Server) {
    if (!this.server) {
      this.server = server;
      this.server.setMaxListeners(100);
      this.wsService.setServer(server);
      console.log('WebSocket server initialized with increased listener limit');
    }
  }

  async handleConnection(client: any, ...args: any[]) {
    try {
      const dt = await this.getClientInfo(client.id);
      if (this.server && !dt) {
        if (!['/auth', '/', ''].includes(client.nsp.name)) {
          const token = client.handshake.headers.token;

          console.log('client', client.id);
          console.log('name', client.nsp.name);

          if (!token) {
            throw new WsException({
              statusCode: 401,
              message: 'TOKEN_REQUIRED',
            });
          }
        }
        console.log(
          `Client connected: ${client.id}:${client.handshake.address
            .split(':')
            .pop()}`,
        );
        const clientIp = client.handshake.address.split(':').pop();
        const clientInfo = {
          id: client.id,
          ip: clientIp,
          status: 'CONNECTED',
        };
        this.storeClientInfo(client.id, clientInfo);
      }
    } catch (error) {
      console.log('Error during connection:', error);
      client.disconnect();
      this.handleDisconnect(client);
    }
  }

  async handleDisconnect(client: any) {
    const dt = await this.getClientInfo(client.id);
    if (this.server && dt) {
      console.log(`Client disconnected: ${client.id}`);
      const clientIp = client.handshake.address.split(':').pop();
      const clientInfo = {
        status: 'DISCONNECTED',
      };
      this.updateClientStatus(client.id, clientInfo);
    }
  }

  private storeClientInfo(socketId: string, clientInfo: any): void {
    // Store client info in Redis
    // You can customize this based on your needs
    const key = `erp_client:${socketId}`;
    const value = JSON.stringify(clientInfo);

    // Use the separate Redis client to publish the data
    this.redisService.set(key, value);
  }

  private getClientInfo(socketId: string) {
    // Store client info in Redis
    // You can customize this based on your needs
    const key = `erp_client:${socketId}`;

    // Use the separate Redis client to publish the data
    return this.redisService.get(key);
  }

  private updateClientStatus(socketId: string, status: any): void {
    // Update client status in Redis
    // You can customize this based on your needs
    const key = `erp_client:${socketId}`;

    // Use the separate Redis client to update the status
    this.redisService.statusUpdate(key, status);
  }

  @SubscribeMessage('events')
  handleEvent(@MessageBody() data: unknown) {
    const event = 'events';
    console.log('Event called', data);
    return data;
  }

  @SubscribeMessage('disconnectClient')
  async handleDisconnectClient(
    @MessageBody() data: any,
    @ConnectedSocket() client: Socket,
  ) {
    const req = GetJsonFromString(data?.body);

    const clientInfo = {
      status: 'DISCONNECTED',
    };
    this.updateClientStatus(req.clientId, clientInfo);

    return 'Client Disconnected Successfully';
  }

  @SubscribeMessage('getActiveClient')
  async handleGetActiveClients() {
    const results = [];
    this.server.sockets.adapter.rooms.forEach((m) => {
      m.forEach((k) => {
        results.push(k);
      });
    });
    const res = await this.redisService.getActiveClientInfo(results);
    return res;
  }

  @SubscribeMessage('disConnectNonWorking')
  async handleDisConnectNonWorkingClients() {
    const connectedClients = await this.handleGetActiveClients();
    // console.log(connectedClients);
    await this.redisService.disConnectNonWorkingClients(connectedClients);
    return 'Disconnected the unused clients';
  }

  @SubscribeMessage('clearRedis')
  async handleClearRedisData() {
    this.redisService.clearRedis();
    return 'Radis Cleared';
  }
}
