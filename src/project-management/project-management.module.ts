import { Module } from '@nestjs/common';
import { ProjectPlanningServiceModule } from './project-planning-service/project-planning-service.module';
import { TaskManagementServiceModule } from './task-management-service/task-management-service.module';
import { ResourceManagementModule } from './resource-management/resource-management.module';
import { TimeTrackingManagementModule } from './time-tracking-management/time-tracking-management.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [ProjectPlanningServiceModule, TaskManagementServiceModule, ResourceManagementModule, TimeTrackingManagementModule]
})
export class ProjectManagementModule {}
