import { Module } from '@nestjs/common';
import { TimeTrackingManagementService } from './time-tracking-management.service';
import { TimeTrackingManagementGateway } from './time-tracking-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [TimeTrackingManagementGateway, TimeTrackingManagementService],
})
export class TimeTrackingManagementModule {}
