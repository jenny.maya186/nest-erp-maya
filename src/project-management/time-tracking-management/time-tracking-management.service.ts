import { Injectable } from '@nestjs/common';
import { CreateTimeTrackingManagementDto } from './dto/create-time-tracking-management.dto';
import { UpdateTimeTrackingManagementDto } from './dto/update-time-tracking-management.dto';

@Injectable()
export class TimeTrackingManagementService {
  create(createTimeTrackingManagementDto: CreateTimeTrackingManagementDto) {
    return 'This action adds a new timeTrackingManagement';
  }

  findAll() {
    return `This action returns all timeTrackingManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} timeTrackingManagement`;
  }

  update(
    id: number,
    updateTimeTrackingManagementDto: UpdateTimeTrackingManagementDto,
  ) {
    return `This action updates a #${id} timeTrackingManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} timeTrackingManagement`;
  }
}
