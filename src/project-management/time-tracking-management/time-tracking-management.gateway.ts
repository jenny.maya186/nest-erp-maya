import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { TimeTrackingManagementService } from './time-tracking-management.service';
import { CreateTimeTrackingManagementDto } from './dto/create-time-tracking-management.dto';
import { UpdateTimeTrackingManagementDto } from './dto/update-time-tracking-management.dto';
import { BaseGateway } from 'src/base.gateway';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({
  namespace: 'time-tracking-management',
  path: 'time-tracking-management',
  transports: 'time-tracking-management',
})
export class TimeTrackingManagementGateway extends BaseGateway {
  private readonly namespace = 'time-tracking-management';
  constructor(
    private readonly timeTrackingManagementService: TimeTrackingManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createTimeTrackingManagement')
  create(
    @MessageBody()
    createTimeTrackingManagementDto: CreateTimeTrackingManagementDto,
  ) {
    return this.timeTrackingManagementService.create(
      createTimeTrackingManagementDto,
    );
  }

  @SubscribeMessage('findAllTimeTrackingManagement')
  findAll() {
    return this.timeTrackingManagementService.findAll();
  }

  @SubscribeMessage('findOneTimeTrackingManagement')
  findOne(@MessageBody() id: number) {
    return this.timeTrackingManagementService.findOne(id);
  }

  @SubscribeMessage('updateTimeTrackingManagement')
  update(
    @MessageBody()
    updateTimeTrackingManagementDto: UpdateTimeTrackingManagementDto,
  ) {
    return this.timeTrackingManagementService.update(
      updateTimeTrackingManagementDto.id,
      updateTimeTrackingManagementDto,
    );
  }

  @SubscribeMessage('removeTimeTrackingManagement')
  remove(@MessageBody() id: number) {
    return this.timeTrackingManagementService.remove(id);
  }
}
