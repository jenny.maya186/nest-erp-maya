import { PartialType } from '@nestjs/mapped-types';
import { CreateTimeTrackingManagementDto } from './create-time-tracking-management.dto';

export class UpdateTimeTrackingManagementDto extends PartialType(CreateTimeTrackingManagementDto) {
  id: number;
}
