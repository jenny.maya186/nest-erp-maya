import { Module } from '@nestjs/common';
import { ResourceManagementService } from './resource-management.service';
import { ResourceManagementGateway } from './resource-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ResourceManagementGateway, ResourceManagementService],
})
export class ResourceManagementModule {}
