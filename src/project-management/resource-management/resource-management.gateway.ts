import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ResourceManagementService } from './resource-management.service';
import { CreateResourceManagementDto } from './dto/create-resource-management.dto';
import { UpdateResourceManagementDto } from './dto/update-resource-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'resource' })
export class ResourceManagementGateway extends BaseGateway {
  constructor(
    private readonly resourceManagementService: ResourceManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createResourceManagement')
  create(
    @MessageBody() createResourceManagementDto: CreateResourceManagementDto,
  ) {
    return this.resourceManagementService.create(createResourceManagementDto);
  }

  @SubscribeMessage('findAllResourceManagement')
  findAll() {
    return this.resourceManagementService.findAll();
  }

  @SubscribeMessage('findOneResourceManagement')
  findOne(@MessageBody() id: number) {
    return this.resourceManagementService.findOne(id);
  }

  @SubscribeMessage('updateResourceManagement')
  update(
    @MessageBody() updateResourceManagementDto: UpdateResourceManagementDto,
  ) {
    return this.resourceManagementService.update(
      updateResourceManagementDto.id,
      updateResourceManagementDto,
    );
  }

  @SubscribeMessage('removeResourceManagement')
  remove(@MessageBody() id: number) {
    return this.resourceManagementService.remove(id);
  }
}
