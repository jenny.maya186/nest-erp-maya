import { Injectable } from '@nestjs/common';
import { CreateResourceManagementDto } from './dto/create-resource-management.dto';
import { UpdateResourceManagementDto } from './dto/update-resource-management.dto';

@Injectable()
export class ResourceManagementService {
  create(createResourceManagementDto: CreateResourceManagementDto) {
    return 'This action adds a new resourceManagement';
  }

  findAll() {
    return `This action returns all resourceManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} resourceManagement`;
  }

  update(id: number, updateResourceManagementDto: UpdateResourceManagementDto) {
    return `This action updates a #${id} resourceManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} resourceManagement`;
  }
}
