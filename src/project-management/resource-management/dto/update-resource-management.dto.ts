import { PartialType } from '@nestjs/mapped-types';
import { CreateResourceManagementDto } from './create-resource-management.dto';

export class UpdateResourceManagementDto extends PartialType(CreateResourceManagementDto) {
  id: number;
}
