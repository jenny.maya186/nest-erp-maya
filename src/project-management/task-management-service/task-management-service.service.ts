import { Injectable } from '@nestjs/common';
import { CreateTaskManagementServiceDto } from './dto/create-task-management-service.dto';
import { UpdateTaskManagementServiceDto } from './dto/update-task-management-service.dto';

@Injectable()
export class TaskManagementServiceService {
  create(createTaskManagementServiceDto: CreateTaskManagementServiceDto) {
    return 'This action adds a new taskManagementService';
  }

  findAll() {
    return `This action returns all taskManagementService`;
  }

  findOne(id: number) {
    return `This action returns a #${id} taskManagementService`;
  }

  update(id: number, updateTaskManagementServiceDto: UpdateTaskManagementServiceDto) {
    return `This action updates a #${id} taskManagementService`;
  }

  remove(id: number) {
    return `This action removes a #${id} taskManagementService`;
  }
}
