import { PartialType } from '@nestjs/mapped-types';
import { CreateTaskManagementServiceDto } from './create-task-management-service.dto';

export class UpdateTaskManagementServiceDto extends PartialType(CreateTaskManagementServiceDto) {
  id: number;
}
