import { Module } from '@nestjs/common';
import { TaskManagementServiceService } from './task-management-service.service';
import { TaskManagementServiceGateway } from './task-management-service.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [TaskManagementServiceGateway, TaskManagementServiceService],
})
export class TaskManagementServiceModule {}
