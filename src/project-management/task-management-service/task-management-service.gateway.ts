import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { TaskManagementServiceService } from './task-management-service.service';
import { CreateTaskManagementServiceDto } from './dto/create-task-management-service.dto';
import { UpdateTaskManagementServiceDto } from './dto/update-task-management-service.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'task-management' })
export class TaskManagementServiceGateway extends BaseGateway {
  constructor(
    private readonly taskManagementServiceService: TaskManagementServiceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createTaskManagementService')
  create(
    @MessageBody()
    createTaskManagementServiceDto: CreateTaskManagementServiceDto,
  ) {
    return this.taskManagementServiceService.create(
      createTaskManagementServiceDto,
    );
  }

  @SubscribeMessage('findAllTaskManagementService')
  findAll() {
    return this.taskManagementServiceService.findAll();
  }

  @SubscribeMessage('findOneTaskManagementService')
  findOne(@MessageBody() id: number) {
    return this.taskManagementServiceService.findOne(id);
  }

  @SubscribeMessage('updateTaskManagementService')
  update(
    @MessageBody()
    updateTaskManagementServiceDto: UpdateTaskManagementServiceDto,
  ) {
    return this.taskManagementServiceService.update(
      updateTaskManagementServiceDto.id,
      updateTaskManagementServiceDto,
    );
  }

  @SubscribeMessage('removeTaskManagementService')
  remove(@MessageBody() id: number) {
    return this.taskManagementServiceService.remove(id);
  }
}
