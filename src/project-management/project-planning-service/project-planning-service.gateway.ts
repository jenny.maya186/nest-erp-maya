import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ProjectPlanningServiceService } from './project-planning-service.service';
import { CreateProjectPlanningServiceDto } from './dto/create-project-planning-service.dto';
import { UpdateProjectPlanningServiceDto } from './dto/update-project-planning-service.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'project-planning' })
export class ProjectPlanningServiceGateway extends BaseGateway {
  constructor(
    private readonly projectPlanningServiceService: ProjectPlanningServiceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createProjectPlanningService')
  create(
    @MessageBody()
    createProjectPlanningServiceDto: CreateProjectPlanningServiceDto,
  ) {
    return this.projectPlanningServiceService.create(
      createProjectPlanningServiceDto,
    );
  }

  @SubscribeMessage('findAllProjectPlanningService')
  findAll() {
    return this.projectPlanningServiceService.findAll();
  }

  @SubscribeMessage('findOneProjectPlanningService')
  findOne(@MessageBody() id: number) {
    return this.projectPlanningServiceService.findOne(id);
  }

  @SubscribeMessage('updateProjectPlanningService')
  update(
    @MessageBody()
    updateProjectPlanningServiceDto: UpdateProjectPlanningServiceDto,
  ) {
    return this.projectPlanningServiceService.update(
      updateProjectPlanningServiceDto.id,
      updateProjectPlanningServiceDto,
    );
  }

  @SubscribeMessage('removeProjectPlanningService')
  remove(@MessageBody() id: number) {
    return this.projectPlanningServiceService.remove(id);
  }
}
