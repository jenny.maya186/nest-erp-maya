import { Module } from '@nestjs/common';
import { ProjectPlanningServiceService } from './project-planning-service.service';
import { ProjectPlanningServiceGateway } from './project-planning-service.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ProjectPlanningServiceGateway, ProjectPlanningServiceService],
})
export class ProjectPlanningServiceModule {}
