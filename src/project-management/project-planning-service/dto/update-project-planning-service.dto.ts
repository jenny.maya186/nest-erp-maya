import { PartialType } from '@nestjs/mapped-types';
import { CreateProjectPlanningServiceDto } from './create-project-planning-service.dto';

export class UpdateProjectPlanningServiceDto extends PartialType(CreateProjectPlanningServiceDto) {
  id: number;
}
