import { Injectable } from '@nestjs/common';
import { CreateProjectPlanningServiceDto } from './dto/create-project-planning-service.dto';
import { UpdateProjectPlanningServiceDto } from './dto/update-project-planning-service.dto';

@Injectable()
export class ProjectPlanningServiceService {
  create(createProjectPlanningServiceDto: CreateProjectPlanningServiceDto) {
    return 'This action adds a new projectPlanningService';
  }

  findAll() {
    return `This action returns all projectPlanningService`;
  }

  findOne(id: number) {
    return `This action returns a #${id} projectPlanningService`;
  }

  update(id: number, updateProjectPlanningServiceDto: UpdateProjectPlanningServiceDto) {
    return `This action updates a #${id} projectPlanningService`;
  }

  remove(id: number) {
    return `This action removes a #${id} projectPlanningService`;
  }
}
