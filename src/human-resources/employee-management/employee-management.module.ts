import { Module } from '@nestjs/common';
import { EmployeeManagementService } from './employee-management.service';
import { EmployeeManagementGateway } from './employee-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [EmployeeManagementGateway, EmployeeManagementService],
})
export class EmployeeManagementModule {}
