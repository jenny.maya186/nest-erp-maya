import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { EmployeeManagementService } from './employee-management.service';
import { CreateEmployeeManagementDto } from './dto/create-employee-management.dto';
import { UpdateEmployeeManagementDto } from './dto/update-employee-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'employee' })
export class EmployeeManagementGateway extends BaseGateway {
  constructor(
    private readonly employeeManagementService: EmployeeManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createEmployeeManagement')
  create(
    @MessageBody() createEmployeeManagementDto: CreateEmployeeManagementDto,
  ) {
    return this.employeeManagementService.create(createEmployeeManagementDto);
  }

  @SubscribeMessage('findAllEmployeeManagement')
  findAll() {
    return this.employeeManagementService.findAll();
  }

  @SubscribeMessage('findOneEmployeeManagement')
  findOne(@MessageBody() id: number) {
    return this.employeeManagementService.findOne(id);
  }

  @SubscribeMessage('updateEmployeeManagement')
  update(
    @MessageBody() updateEmployeeManagementDto: UpdateEmployeeManagementDto,
  ) {
    return this.employeeManagementService.update(
      updateEmployeeManagementDto.id,
      updateEmployeeManagementDto,
    );
  }

  @SubscribeMessage('removeEmployeeManagement')
  remove(@MessageBody() id: number) {
    return this.employeeManagementService.remove(id);
  }
}
