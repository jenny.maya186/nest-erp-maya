import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { RecruitmentService } from './recruitment.service';
import { CreateRecruitmentDto } from './dto/create-recruitment.dto';
import { UpdateRecruitmentDto } from './dto/update-recruitment.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'recruitment' })
export class RecruitmentGateway extends BaseGateway {
  constructor(
    private readonly recruitmentService: RecruitmentService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createRecruitment')
  create(@MessageBody() createRecruitmentDto: CreateRecruitmentDto) {
    return this.recruitmentService.create(createRecruitmentDto);
  }

  @SubscribeMessage('findAllRecruitment')
  findAll() {
    return this.recruitmentService.findAll();
  }

  @SubscribeMessage('findOneRecruitment')
  findOne(@MessageBody() id: number) {
    return this.recruitmentService.findOne(id);
  }

  @SubscribeMessage('updateRecruitment')
  update(@MessageBody() updateRecruitmentDto: UpdateRecruitmentDto) {
    return this.recruitmentService.update(
      updateRecruitmentDto.id,
      updateRecruitmentDto,
    );
  }

  @SubscribeMessage('removeRecruitment')
  remove(@MessageBody() id: number) {
    return this.recruitmentService.remove(id);
  }
}
