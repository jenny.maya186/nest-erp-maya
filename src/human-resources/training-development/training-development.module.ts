import { Module } from '@nestjs/common';
import { TrainingDevelopmentService } from './training-development.service';
import { TrainingDevelopmentGateway } from './training-development.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [TrainingDevelopmentGateway, TrainingDevelopmentService],
})
export class TrainingDevelopmentModule {}
