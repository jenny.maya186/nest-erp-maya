import { Injectable } from '@nestjs/common';
import { CreateTrainingDevelopmentDto } from './dto/create-training-development.dto';
import { UpdateTrainingDevelopmentDto } from './dto/update-training-development.dto';

@Injectable()
export class TrainingDevelopmentService {
  create(createTrainingDevelopmentDto: CreateTrainingDevelopmentDto) {
    return 'This action adds a new trainingDevelopment';
  }

  findAll() {
    return `This action returns all trainingDevelopment`;
  }

  findOne(id: number) {
    return `This action returns a #${id} trainingDevelopment`;
  }

  update(id: number, updateTrainingDevelopmentDto: UpdateTrainingDevelopmentDto) {
    return `This action updates a #${id} trainingDevelopment`;
  }

  remove(id: number) {
    return `This action removes a #${id} trainingDevelopment`;
  }
}
