import { PartialType } from '@nestjs/mapped-types';
import { CreateTrainingDevelopmentDto } from './create-training-development.dto';

export class UpdateTrainingDevelopmentDto extends PartialType(CreateTrainingDevelopmentDto) {
  id: number;
}
