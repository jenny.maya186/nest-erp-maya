import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { TrainingDevelopmentService } from './training-development.service';
import { CreateTrainingDevelopmentDto } from './dto/create-training-development.dto';
import { UpdateTrainingDevelopmentDto } from './dto/update-training-development.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'training-development' })
export class TrainingDevelopmentGateway extends BaseGateway {
  constructor(
    private readonly trainingDevelopmentService: TrainingDevelopmentService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createTrainingDevelopment')
  create(
    @MessageBody() createTrainingDevelopmentDto: CreateTrainingDevelopmentDto,
  ) {
    return this.trainingDevelopmentService.create(createTrainingDevelopmentDto);
  }

  @SubscribeMessage('findAllTrainingDevelopment')
  findAll() {
    return this.trainingDevelopmentService.findAll();
  }

  @SubscribeMessage('findOneTrainingDevelopment')
  findOne(@MessageBody() id: number) {
    return this.trainingDevelopmentService.findOne(id);
  }

  @SubscribeMessage('updateTrainingDevelopment')
  update(
    @MessageBody() updateTrainingDevelopmentDto: UpdateTrainingDevelopmentDto,
  ) {
    return this.trainingDevelopmentService.update(
      updateTrainingDevelopmentDto.id,
      updateTrainingDevelopmentDto,
    );
  }

  @SubscribeMessage('removeTrainingDevelopment')
  remove(@MessageBody() id: number) {
    return this.trainingDevelopmentService.remove(id);
  }
}
