import { PartialType } from '@nestjs/mapped-types';
import { CreatePerformanceManagementDto } from './create-performance-management.dto';

export class UpdatePerformanceManagementDto extends PartialType(CreatePerformanceManagementDto) {
  id: number;
}
