import { Module } from '@nestjs/common';
import { PerformanceManagementService } from './performance-management.service';
import { PerformanceManagementGateway } from './performance-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [PerformanceManagementGateway, PerformanceManagementService],
})
export class PerformanceManagementModule {}
