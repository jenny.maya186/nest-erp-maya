import { Injectable } from '@nestjs/common';
import { CreatePerformanceManagementDto } from './dto/create-performance-management.dto';
import { UpdatePerformanceManagementDto } from './dto/update-performance-management.dto';

@Injectable()
export class PerformanceManagementService {
  create(createPerformanceManagementDto: CreatePerformanceManagementDto) {
    return 'This action adds a new performanceManagement';
  }

  findAll() {
    return `This action returns all performanceManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} performanceManagement`;
  }

  update(id: number, updatePerformanceManagementDto: UpdatePerformanceManagementDto) {
    return `This action updates a #${id} performanceManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} performanceManagement`;
  }
}
