import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { PerformanceManagementService } from './performance-management.service';
import { CreatePerformanceManagementDto } from './dto/create-performance-management.dto';
import { UpdatePerformanceManagementDto } from './dto/update-performance-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'performance' })
export class PerformanceManagementGateway extends BaseGateway {
  constructor(
    private readonly performanceManagementService: PerformanceManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createPerformanceManagement')
  create(
    @MessageBody()
    createPerformanceManagementDto: CreatePerformanceManagementDto,
  ) {
    return this.performanceManagementService.create(
      createPerformanceManagementDto,
    );
  }

  @SubscribeMessage('findAllPerformanceManagement')
  findAll() {
    return this.performanceManagementService.findAll();
  }

  @SubscribeMessage('findOnePerformanceManagement')
  findOne(@MessageBody() id: number) {
    return this.performanceManagementService.findOne(id);
  }

  @SubscribeMessage('updatePerformanceManagement')
  update(
    @MessageBody()
    updatePerformanceManagementDto: UpdatePerformanceManagementDto,
  ) {
    return this.performanceManagementService.update(
      updatePerformanceManagementDto.id,
      updatePerformanceManagementDto,
    );
  }

  @SubscribeMessage('removePerformanceManagement')
  remove(@MessageBody() id: number) {
    return this.performanceManagementService.remove(id);
  }
}
