import { PartialType } from '@nestjs/mapped-types';
import { CreateTimeAttendanceDto } from './create-time-attendance.dto';

export class UpdateTimeAttendanceDto extends PartialType(CreateTimeAttendanceDto) {
  id: number;
}
