import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { TimeAttendanceService } from './time-attendance.service';
import { CreateTimeAttendanceDto } from './dto/create-time-attendance.dto';
import { UpdateTimeAttendanceDto } from './dto/update-time-attendance.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'attendance' })
export class TimeAttendanceGateway extends BaseGateway {
  constructor(
    private readonly timeAttendanceService: TimeAttendanceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createTimeAttendance')
  create(@MessageBody() createTimeAttendanceDto: CreateTimeAttendanceDto) {
    return this.timeAttendanceService.create(createTimeAttendanceDto);
  }

  @SubscribeMessage('findAllTimeAttendance')
  findAll() {
    return this.timeAttendanceService.findAll();
  }

  @SubscribeMessage('findOneTimeAttendance')
  findOne(@MessageBody() id: number) {
    return this.timeAttendanceService.findOne(id);
  }

  @SubscribeMessage('updateTimeAttendance')
  update(@MessageBody() updateTimeAttendanceDto: UpdateTimeAttendanceDto) {
    return this.timeAttendanceService.update(
      updateTimeAttendanceDto.id,
      updateTimeAttendanceDto,
    );
  }

  @SubscribeMessage('removeTimeAttendance')
  remove(@MessageBody() id: number) {
    return this.timeAttendanceService.remove(id);
  }
}
