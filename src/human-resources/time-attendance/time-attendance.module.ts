import { Module } from '@nestjs/common';
import { TimeAttendanceService } from './time-attendance.service';
import { TimeAttendanceGateway } from './time-attendance.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [TimeAttendanceGateway, TimeAttendanceService],
})
export class TimeAttendanceModule {}
