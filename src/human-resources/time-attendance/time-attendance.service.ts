import { Injectable } from '@nestjs/common';
import { CreateTimeAttendanceDto } from './dto/create-time-attendance.dto';
import { UpdateTimeAttendanceDto } from './dto/update-time-attendance.dto';

@Injectable()
export class TimeAttendanceService {
  create(createTimeAttendanceDto: CreateTimeAttendanceDto) {
    return 'This action adds a new timeAttendance';
  }

  findAll() {
    return `This action returns all timeAttendance`;
  }

  findOne(id: number) {
    return `This action returns a #${id} timeAttendance`;
  }

  update(id: number, updateTimeAttendanceDto: UpdateTimeAttendanceDto) {
    return `This action updates a #${id} timeAttendance`;
  }

  remove(id: number) {
    return `This action removes a #${id} timeAttendance`;
  }
}
