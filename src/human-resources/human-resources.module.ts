import { Module } from '@nestjs/common';
import { EmployeeManagementModule } from './employee-management/employee-management.module';
import { RecruitmentModule } from './recruitment/recruitment.module';
import { PerformanceManagementModule } from './performance-management/performance-management.module';
import { TrainingDevelopmentModule } from './training-development/training-development.module';
import { TimeAttendanceModule } from './time-attendance/time-attendance.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [EmployeeManagementModule, RecruitmentModule, PerformanceManagementModule, TrainingDevelopmentModule, TimeAttendanceModule]
})
export class HumanResourcesModule {}
