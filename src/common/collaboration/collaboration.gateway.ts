import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { CollaborationService } from './collaboration.service';
import { CreateCollaborationDto } from './dto/create-collaboration.dto';
import { UpdateCollaborationDto } from './dto/update-collaboration.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'collaboration' })
export class CollaborationGateway extends BaseGateway {
  constructor(
    private readonly collaborationService: CollaborationService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createCollaboration')
  create(@MessageBody() createCollaborationDto: CreateCollaborationDto) {
    return this.collaborationService.create(createCollaborationDto);
  }

  @SubscribeMessage('findAllCollaboration')
  findAll() {
    return this.collaborationService.findAll();
  }

  @SubscribeMessage('findOneCollaboration')
  findOne(@MessageBody() id: number) {
    return this.collaborationService.findOne(id);
  }

  @SubscribeMessage('updateCollaboration')
  update(@MessageBody() updateCollaborationDto: UpdateCollaborationDto) {
    return this.collaborationService.update(
      updateCollaborationDto.id,
      updateCollaborationDto,
    );
  }

  @SubscribeMessage('removeCollaboration')
  remove(@MessageBody() id: number) {
    return this.collaborationService.remove(id);
  }
}
