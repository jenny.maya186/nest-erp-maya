import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { splitDateRange } from '../utils/helper.utils';
import { Log } from './entities/log.entity';
import { SearchOperationLogDto } from './dto/search-operation-log.dto';
import { ApiResponse } from '../utils/response.util';

export class LogRepository extends Repository<Log> {
  constructor(
    @InjectRepository(Log)
    private logRepo: Repository<Log>,
  ) {
    super(logRepo.target, logRepo.manager, logRepo.queryRunner);
  }

  async operationLogs(req: SearchOperationLogDto) {
    const { username, operationModule, ipAddress, mobileNumber, createdDate } =
      req;
    const limit =
      req.limit && !isNaN(req.limit) && req.limit > 0 ? req.limit : 10;
    const offset =
      req.offset && !isNaN(req.offset) && req.offset >= 0 ? req.offset : 0;

    const query = this.logRepo.createQueryBuilder('log');

    if (username) {
      query.andWhere(' log.requested_by LIKE :username ', {
        username: `%${username}%`,
      });
    }
    if (operationModule) {
      query.andWhere(
        '(log.admin_page LIKE :operationModule OR log.action LIKE :operationModule)',
        {
          operationModule: `%${operationModule}%`,
        },
      );
    }

    if (ipAddress) {
      query.andWhere('log.ip_address = :ip_address', { ipAddress });
    }

    if (mobileNumber) {
      query.andWhere('log.mobile_number = :mobile_number', { mobileNumber });
    }

    if (createdDate) {
      const { startDate, endDate } = splitDateRange(createdDate);
      query.andWhere('(log.created_at BETWEEN :startDate AND :endDate)', {
        startDate: startDate,
        endDate: endDate,
      });
    }

    query.andWhere('log.action NOT IN ("CREATE_LOG","CREATE_BROADCAST")');

    const list = await query
      .select([
        'log.id as id',
        'log.browser  as browser',
        'log.ip_address as ip_address',
        'log.requested_by as requested_by',
        'log.admin_page as admin_page',
        'log.action as action',
        'log.created_at as created_at',
        'log.os as os',
        'log.platform as platform',
        'log.version as version',
        'log.response_time as response_time',
        'log.status_code as status_code',
        'log.mobile_number as mobile_number',
      ])
      .orderBy('log.id', 'DESC')
      .offset(offset)
      .limit(limit)
      .getRawMany();
    const count = await query.getCount();

    return ApiResponse(
      {
        list,
        count,
      },
      200,
      'Fetch Operation Logs',
    );
  }
}
