import { Module } from '@nestjs/common';
import { AuditService } from './audit.service';
import { AuditGateway } from './audit.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [AuditGateway, AuditService],
})
export class AuditModule {}
