import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { AuditService } from './audit.service';
import { CreateAuditDto } from './dto/create-audit.dto';
import { UpdateAuditDto } from './dto/update-audit.dto';
import { BaseGateway } from 'src/base.gateway';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'audit' })
export class AuditGateway extends BaseGateway {
  constructor(
    private readonly auditService: AuditService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createAudit')
  create(@MessageBody() createAuditDto: CreateAuditDto) {
    return this.auditService.create(createAuditDto);
  }

  @SubscribeMessage('findAllAudit')
  findAll() {
    return this.auditService.findAll();
  }

  @SubscribeMessage('findOneAudit')
  findOne(@MessageBody() id: number) {
    return this.auditService.findOne(id);
  }

  @SubscribeMessage('updateAudit')
  update(@MessageBody() updateAuditDto: UpdateAuditDto) {
    return this.auditService.update(updateAuditDto.id, updateAuditDto);
  }

  @SubscribeMessage('removeAudit')
  remove(@MessageBody() id: number) {
    return this.auditService.remove(id);
  }
}
