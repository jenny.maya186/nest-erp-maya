import { Module } from '@nestjs/common';
import { DataSyncService } from './data-sync.service';
import { DataSyncGateway } from './data-sync.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [DataSyncGateway, DataSyncService],
})
export class DataSyncModule {}
