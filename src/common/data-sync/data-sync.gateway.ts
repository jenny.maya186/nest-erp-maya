import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { DataSyncService } from './data-sync.service';
import { CreateDataSyncDto } from './dto/create-data-sync.dto';
import { UpdateDataSyncDto } from './dto/update-data-sync.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'data-sync' })
export class DataSyncGateway extends BaseGateway {
  constructor(
    private readonly dataSyncService: DataSyncService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createDataSync')
  create(@MessageBody() createDataSyncDto: CreateDataSyncDto) {
    return this.dataSyncService.create(createDataSyncDto);
  }

  @SubscribeMessage('findAllDataSync')
  findAll() {
    return this.dataSyncService.findAll();
  }

  @SubscribeMessage('findOneDataSync')
  findOne(@MessageBody() id: number) {
    return this.dataSyncService.findOne(id);
  }

  @SubscribeMessage('updateDataSync')
  update(@MessageBody() updateDataSyncDto: UpdateDataSyncDto) {
    return this.dataSyncService.update(updateDataSyncDto.id, updateDataSyncDto);
  }

  @SubscribeMessage('removeDataSync')
  remove(@MessageBody() id: number) {
    return this.dataSyncService.remove(id);
  }
}
