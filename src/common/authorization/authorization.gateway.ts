import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { AuthorizationService } from './authorization.service';
import { CreateAuthorizationDto } from './dto/create-authorization.dto';
import { UpdateAuthorizationDto } from './dto/update-authorization.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'authorization' })
export class AuthorizationGateway extends BaseGateway {
  constructor(
    private readonly authorizationService: AuthorizationService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createAuthorization')
  create(@MessageBody() createAuthorizationDto: CreateAuthorizationDto) {
    return this.authorizationService.create(createAuthorizationDto);
  }

  @SubscribeMessage('findAllAuthorization')
  findAll() {
    return this.authorizationService.findAll();
  }

  @SubscribeMessage('findOneAuthorization')
  findOne(@MessageBody() id: number) {
    return this.authorizationService.findOne(id);
  }

  @SubscribeMessage('updateAuthorization')
  update(@MessageBody() updateAuthorizationDto: UpdateAuthorizationDto) {
    return this.authorizationService.update(
      updateAuthorizationDto.id,
      updateAuthorizationDto,
    );
  }

  @SubscribeMessage('removeAuthorization')
  remove(@MessageBody() id: number) {
    return this.authorizationService.remove(id);
  }
}
