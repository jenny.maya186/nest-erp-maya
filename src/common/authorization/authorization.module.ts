import { Module } from '@nestjs/common';
import { AuthorizationService } from './authorization.service';
import { AuthorizationGateway } from './authorization.gateway';
import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [AuthorizationGateway, AuthorizationService, BaseGatewayModule],
  exports: [AuthorizationGateway, AuthorizationService, BaseGatewayModule],
})
export class AuthorizationModule {}
