import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { SecurityService } from './security.service';
import { CreateSecurityDto } from './dto/create-security.dto';
import { UpdateSecurityDto } from './dto/update-security.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'security' })
export class SecurityGateway extends BaseGateway {
  constructor(
    private readonly securityService: SecurityService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createSecurity')
  create(@MessageBody() createSecurityDto: CreateSecurityDto) {
    return this.securityService.create(createSecurityDto);
  }

  @SubscribeMessage('findAllSecurity')
  findAll() {
    return this.securityService.findAll();
  }

  @SubscribeMessage('findOneSecurity')
  findOne(@MessageBody() id: number) {
    return this.securityService.findOne(id);
  }

  @SubscribeMessage('updateSecurity')
  update(@MessageBody() updateSecurityDto: UpdateSecurityDto) {
    return this.securityService.update(updateSecurityDto.id, updateSecurityDto);
  }

  @SubscribeMessage('removeSecurity')
  remove(@MessageBody() id: number) {
    return this.securityService.remove(id);
  }
}
