import { Module } from '@nestjs/common';
import { SecurityService } from './security.service';
import { SecurityGateway } from './security.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [SecurityGateway, SecurityService],
})
export class SecurityModule {}
