/* eslint-disable @typescript-eslint/ban-types */
import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  ConnectedSocket,
} from '@nestjs/websockets';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/auth/register.dto';
import { LoginDto } from './dto/auth/login.dto';
import { ForgetPasswordDto } from './dto/user/forget-password.dto.ts';
import { ResetPasswordDto } from './dto/user/reset-password.dto';
import { VerifyEmailDto } from './dto/user/verify-email.dto.ts';
import { BaseGateway } from 'src/base.gateway';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';
import { Socket } from 'socket.io';

@WebSocketGateway({ cors: true, namespace: 'auth' })
export class AuthGateway extends BaseGateway {
  constructor(
    private readonly authService: AuthService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('auth.register')
  register(@ConnectedSocket() client: Socket, @MessageBody() req: RegisterDto) {
    return this.authService.register(req);
  }

  @SubscribeMessage('auth.login')
  async login(@MessageBody() req: LoginDto, @ConnectedSocket() client: Socket) {
    return await this.authService.login(req);
  }

  @SubscribeMessage('auth.forgot-password')
  forgetPassword(
    @ConnectedSocket() client: Socket,
    @MessageBody() forgetPasswordDto: ForgetPasswordDto,
  ) {
    return this.authService.forgetPassword(forgetPasswordDto);
  }

  @SubscribeMessage('auth.reset-password')
  resetPassword(
    @ConnectedSocket() client: Socket,
    @MessageBody() resetPasswordDto: ResetPasswordDto,
  ) {
    return this.authService.resetPassword(resetPasswordDto);
  }

  @SubscribeMessage('auth.verify-email')
  verifyEmail(
    @ConnectedSocket() client: Socket,
    @MessageBody() verifyEmaildto: VerifyEmailDto,
  ) {
    return this.authService.verifyEmail(verifyEmaildto);
  }

  @SubscribeMessage('auth.logout')
  logOut(@ConnectedSocket() client: Socket, @MessageBody() req: any) {
    return this.authService.logout(req);
  }

  @SubscribeMessage('auth.info')
  getInfo(@ConnectedSocket() client: any) {
    return this.authService.getInfo(client);
  }
}
