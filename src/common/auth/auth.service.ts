/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LoginDto } from './dto/auth/login.dto';
import { RegisterDto } from './dto/auth/register.dto';
import { ForgetPasswordDto } from './dto/user/forget-password.dto.ts';
import { ResetPasswordDto } from './dto/user/reset-password.dto';
import { SearchUserDto } from './dto/user/search-user.dto';
import { VerifyEmailDto } from './dto/user/verify-email.dto.ts';
import { User } from './entities/user.entity';
import { RoleRepository } from './repository/role.repository';
import { UserRepository } from './repository/user.repository';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserRepository))
    public userRepository: UserRepository,
    @Inject(forwardRef(() => RoleRepository))
    public roleRepository: RoleRepository,
  ) {}

  register(registerDto: RegisterDto) {
    return this.userRepository.register(registerDto);
  }

  async findAll(filterDto: SearchUserDto) {
    return this.userRepository.getUsers(filterDto);
  }

  async findOne(id: number): Promise<User> {
    return this.userRepository.findOne({
      relations: ['roles'],
      where: {
        id: id,
      },
    });
  }

  login(loginDto: LoginDto) {
    return this.userRepository.login(loginDto);
  }
  logout(req: any) {
    return this.userRepository.logout(req);
  }
  async getInfo(client: any) {
    try {
      const token = client.handshake.headers.token;
      if (!token) {
        throw new UnauthorizedException({
          statusCode: 401,
          message: 'TOKEN_REQUIRED',
        });
      }
      const userToken = await this.userRepository.findSessionToken(token);
      if (!userToken) {
        throw new UnauthorizedException({
          statusCode: 401,
          message: 'TOKEN_EXPIRED',
        });
      }
      const user = await this.findOne(userToken?.user?.id);
      if (user) {
        return this.roleRepository.getRoleById(user.roles.id);
      } else {
        throw new NotFoundException('INVALID_TOKEN_USER');
      }
    } catch (error) {
      return error;
    }
  }
  resetPassword(resetPasswordDto: ResetPasswordDto) {
    throw new Error('Method not implemented.');
  }

  verifyEmail(verifyEmaildto: VerifyEmailDto) {
    throw new Error('Method not implemented.');
  }

  forgetPassword(forgetPasswordDto: ForgetPasswordDto) {
    throw new Error('Method not implemented.');
  }
}
