/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { ChangePasswordDto } from './dto/user/change-password.dto';
import { CreateUserDto } from './dto/user/create-user.dto';
import { SearchUserDto } from './dto/user/search-user.dto';
import { UpdateUserDto } from './dto/user/update-user.dto';
import { UserRepository } from './repository/user.repository';
import { ApiResponse } from '../utils/response.util';
import { PermissionService } from './repository/permission.service';

@Injectable()
export class UserService {
  constructor(
    @Inject(forwardRef(() => UserRepository))
    public userRepository: UserRepository,
    @Inject(forwardRef(() => PermissionService))
    public permissionService: PermissionService,
  ) {}
  create(createUserDto: CreateUserDto, userId: number) {
    return this.userRepository.createUser(createUserDto, userId);
  }
  async findAll(filterDto: SearchUserDto) {
    return this.userRepository.getUsers(filterDto);
  }

  async remove(id: number) {
    const result = await this.userRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`User with ID ${id} not found`);
    }
    return ApiResponse(null, 200, 'User Deleted');
  }

  findById(userId: number) {
    return this.userRepository.findOne({
      where: { id: userId },
      relations: ['roles', 'roles.permissions'],
    });
  }
  findByUsername(username: string) {
    return this.userRepository.findOne({
      where: { username: username },
      relations: ['roles', 'roles.permissions'],
    });
  }

  async findOne(id: number) {
    const user = await this.userRepository.findOne({
      where: { id: id },
      relations: ['roles'],
    });
    if (!user) {
      return new NotFoundException('User Not Found with this id');
    }
    const { password, ...others } = user;
    return others;
  }

  canAccess(userId: any, eventName: any): Promise<boolean> {
    return this.permissionService.checkPermission(userId, eventName);
  }

  findSessionToken(toke: string) {
    return this.userRepository.findSessionToken(toke);
  }

  updateExpireInToken(token: string) {
    return this.userRepository.updateExpireInToken(token);
  }

  updateUser(userId: any, updateData: UpdateUserDto, user: any) {
    return this.userRepository.updateUser(userId, updateData, user);
  }

  changePassword(dtoReq: ChangePasswordDto, req: any) {
    return this.userRepository.changePassword(dtoReq, req);
  }
}
