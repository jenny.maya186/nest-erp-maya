// auth.module.ts
import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { AuthGateway } from './auth.gateway';
import { UserRepository } from './repository/user.repository';
import { RoleRepository } from './repository/role.repository';
import { PermissionRepository } from './repository/permission.repository';
import { JwtService } from '@nestjs/jwt';
import { AdminPageRepository } from './repository/admin-page.repository';
import { SessionRepository } from './repository/session.repository';
import { User } from './entities/user.entity';
import { Role } from './entities/role.entity';
import { AdminPage } from './entities/admin-page.entity';
import { Permission } from './entities/permission.entity';
import { Session } from './entities/session.entity';
import { PermissionService } from './repository/permission.service';
import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, AdminPage, Permission, Session]),
    forwardRef(() => BaseGatewayModule),
  ],
  providers: [
    AuthGateway,
    AuthService,
    RoleRepository,
    PermissionRepository,
    SessionRepository,
    AdminPageRepository,
    JwtService,
    UserRepository,
    PermissionService,
  ],
  exports: [
    AuthGateway,
    AuthService,
    RoleRepository,
    PermissionRepository,
    SessionRepository,
    AdminPageRepository,
    JwtService,
    UserRepository,
    PermissionService,
  ],
})
export class AuthModule {}
