import { Repository } from 'typeorm';
import { AdminPage } from '../entities/admin-page.entity';
import { Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

export class AdminPageRepository extends Repository<AdminPage> {
  constructor(
    @InjectRepository(AdminPage)
    private apRepo: Repository<AdminPage>,
  ) {
    super(apRepo.target, apRepo.manager, apRepo.queryRunner);
  }
}
