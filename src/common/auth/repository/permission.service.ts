import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { PermissionRepository } from './permission.repository';
import { UserRepository } from './user.repository';
import {
  CHECK_LOGIN_ROUTES,
  EXCLUDED_ROUTES,
} from 'src/common/utils/helper.utils';

@Injectable()
export class PermissionService {
  constructor(
    @Inject(forwardRef(() => UserRepository))
    private readonly userRepository: UserRepository,
    @Inject(forwardRef(() => PermissionRepository))
    private readonly permissionRepository: PermissionRepository,
  ) {}

  async checkPermission(userId: string, eventName: string): Promise<boolean> {
    if (EXCLUDED_ROUTES.includes(eventName)) {
      return true;
    }
    if (CHECK_LOGIN_ROUTES.includes(eventName)) {
      return true;
    }
    const user = await this.userRepository.findOne({
      where: {
        username: userId,
      },
      relations: ['roles', 'roles.permissions'],
    });

    if (!user) {
      return false; // User not found, return false
    }

    // Check if any of the user's roles have the required permission
    const hasPermission = user.roles.permissions.some(
      (permission) => permission.name === eventName,
    );

    return hasPermission;
  }
}
