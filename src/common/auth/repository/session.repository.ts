import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Session } from '../entities/session.entity';
import { Inject, forwardRef } from '@nestjs/common';

export class SessionRepository extends Repository<Session> {
  constructor(
    @InjectRepository(Session)
    private sessionRepository: Repository<Session>,
  ) {
    super(
      sessionRepository.target,
      sessionRepository.manager,
      sessionRepository.queryRunner,
    );
  }
}
