import { IsNotEmpty } from 'class-validator';

export class LoginDto {
  @IsNotEmpty({
    message: 'USERNAME_IS REQUIRED',
  })
  username: string;

  @IsNotEmpty({
    message: 'PASSWORD_IS REQUIRED',
  })
  password: string;
}
