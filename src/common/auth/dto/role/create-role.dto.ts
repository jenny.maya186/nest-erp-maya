import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { AdminPagePermissionsDto } from '../permission/admin-page-permissions.dto';

export class CreateRoleDto {
  @IsString({
    message: 'NAME_IS_STRING',
  })
  @IsNotEmpty({
    message: 'NAME_IS_REQUIRED',
  })
  name: string;

  @IsString({
    message: 'DESCRIPTION_IS_STRING',
  })
  @IsNotEmpty({
    message: 'DESCRIPTION_IS_REQUIRED',
  })
  description: string;

  @IsNotEmpty({
    message: 'PERMISSION_IS_REQUIRED',
  })
  @IsArray({
    message: 'PERMISSION_IS_ARRAY',
  })
  permissions: AdminPagePermissionsDto[];
}
