/* eslint-disable prettier/prettier */
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsIn, IsOptional } from 'class-validator';
import { SearchBaseDto } from 'src/dto/search-base-dto';
import { UserStatus } from 'src/enum/user-status.enum';

export class SearchUserDto extends SearchBaseDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsIn([UserStatus.ACTIVE, UserStatus.INACTIVE, ''])
  status: UserStatus;

  @ApiPropertyOptional()
  @IsOptional()
  search: string;
}
