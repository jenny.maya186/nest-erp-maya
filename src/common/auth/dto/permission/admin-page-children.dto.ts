import { IsArray, IsNumber } from 'class-validator';

export class AdminPageChildrenDto {
  @IsNumber()
  childrenId: number;

  @IsArray({
    message: 'PERMISSION_IS_ARRAY',
  })
  permissions: number[];
}
