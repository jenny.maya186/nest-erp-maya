import { IsArray, IsNumber } from 'class-validator';
import { AdminPageChildrenDto } from './admin-page-children.dto';

export class AdminPagePermissionsDto {
  @IsNumber()
  adminPageId: number;

  @IsArray()
  children: AdminPageChildrenDto[];
}
