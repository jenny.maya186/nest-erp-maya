import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { AuthorizationModule } from './authorization/authorization.module';
import { DataSyncModule } from './data-sync/data-sync.module';
import { NotifyModule } from './notify/notify.module';
import { CollaborationModule } from './collaboration/collaboration.module';
import { AuditModule } from './audit/audit.module';
import { SecurityModule } from './security/security.module';
import { ComplianceModule } from './compliance/compliance.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [
    AuthModule,
    AuthorizationModule,
    DataSyncModule,
    NotifyModule,
    CollaborationModule,
    AuditModule,
    SecurityModule,
    ComplianceModule,
  ],
  exports: [
    AuthModule,
    AuthorizationModule,
    DataSyncModule,
    NotifyModule,
    CollaborationModule,
    AuditModule,
    SecurityModule,
    ComplianceModule,
  ],
})
export class CommonModule {}
