import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { NotifyService } from './notify.service';
import { CreateNotifyDto } from './dto/create-notify.dto';
import { UpdateNotifyDto } from './dto/update-notify.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'notify' })
export class NotifyGateway extends BaseGateway {
  constructor(
    private readonly notifyService: NotifyService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createNotify')
  create(@MessageBody() createNotifyDto: CreateNotifyDto) {
    return this.notifyService.create(createNotifyDto);
  }

  @SubscribeMessage('findAllNotify')
  findAll() {
    return this.notifyService.findAll();
  }

  @SubscribeMessage('findOneNotify')
  findOne(@MessageBody() id: number) {
    return this.notifyService.findOne(id);
  }

  @SubscribeMessage('updateNotify')
  update(@MessageBody() updateNotifyDto: UpdateNotifyDto) {
    return this.notifyService.update(updateNotifyDto.id, updateNotifyDto);
  }

  @SubscribeMessage('removeNotify')
  remove(@MessageBody() id: number) {
    return this.notifyService.remove(id);
  }
}
