import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { ComplianceService } from './compliance.service';
import { CreateComplianceDto } from './dto/create-compliance.dto';
import { UpdateComplianceDto } from './dto/update-compliance.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'compliance' })
export class ComplianceGateway extends BaseGateway {
  constructor(
    private readonly complianceService: ComplianceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createCompliance')
  create(@MessageBody() createComplianceDto: CreateComplianceDto) {
    return this.complianceService.create(createComplianceDto);
  }

  @SubscribeMessage('findAllCompliance')
  findAll() {
    return this.complianceService.findAll();
  }

  @SubscribeMessage('findOneCompliance')
  findOne(@MessageBody() id: number) {
    return this.complianceService.findOne(id);
  }

  @SubscribeMessage('updateCompliance')
  update(@MessageBody() updateComplianceDto: UpdateComplianceDto) {
    return this.complianceService.update(
      updateComplianceDto.id,
      updateComplianceDto,
    );
  }

  @SubscribeMessage('removeCompliance')
  remove(@MessageBody() id: number) {
    return this.complianceService.remove(id);
  }
}
