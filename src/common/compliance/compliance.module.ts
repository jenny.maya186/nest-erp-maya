import { Module } from '@nestjs/common';
import { ComplianceService } from './compliance.service';
import { ComplianceGateway } from './compliance.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [ComplianceGateway, ComplianceService],
})
export class ComplianceModule {}
