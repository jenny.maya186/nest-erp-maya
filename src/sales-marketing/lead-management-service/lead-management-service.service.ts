import { Injectable } from '@nestjs/common';
import { CreateLeadManagementServiceDto } from './dto/create-lead-management-service.dto';
import { UpdateLeadManagementServiceDto } from './dto/update-lead-management-service.dto';

@Injectable()
export class LeadManagementServiceService {
  create(createLeadManagementServiceDto: CreateLeadManagementServiceDto) {
    return 'This action adds a new leadManagementService';
  }

  findAll() {
    return `This action returns all leadManagementService`;
  }

  findOne(id: number) {
    return `This action returns a #${id} leadManagementService`;
  }

  update(id: number, updateLeadManagementServiceDto: UpdateLeadManagementServiceDto) {
    return `This action updates a #${id} leadManagementService`;
  }

  remove(id: number) {
    return `This action removes a #${id} leadManagementService`;
  }
}
