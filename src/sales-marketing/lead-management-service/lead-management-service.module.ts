import { Module } from '@nestjs/common';
import { LeadManagementServiceService } from './lead-management-service.service';
import { LeadManagementServiceGateway } from './lead-management-service.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [LeadManagementServiceGateway, LeadManagementServiceService],
})
export class LeadManagementServiceModule {}
