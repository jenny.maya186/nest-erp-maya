import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { LeadManagementServiceService } from './lead-management-service.service';
import { CreateLeadManagementServiceDto } from './dto/create-lead-management-service.dto';
import { UpdateLeadManagementServiceDto } from './dto/update-lead-management-service.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'lead-management' })
export class LeadManagementServiceGateway extends BaseGateway {
  constructor(
    private readonly leadManagementServiceService: LeadManagementServiceService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createLeadManagementService')
  create(
    @MessageBody()
    createLeadManagementServiceDto: CreateLeadManagementServiceDto,
  ) {
    return this.leadManagementServiceService.create(
      createLeadManagementServiceDto,
    );
  }

  @SubscribeMessage('findAllLeadManagementService')
  findAll() {
    return this.leadManagementServiceService.findAll();
  }

  @SubscribeMessage('findOneLeadManagementService')
  findOne(@MessageBody() id: number) {
    return this.leadManagementServiceService.findOne(id);
  }

  @SubscribeMessage('updateLeadManagementService')
  update(
    @MessageBody()
    updateLeadManagementServiceDto: UpdateLeadManagementServiceDto,
  ) {
    return this.leadManagementServiceService.update(
      updateLeadManagementServiceDto.id,
      updateLeadManagementServiceDto,
    );
  }

  @SubscribeMessage('removeLeadManagementService')
  remove(@MessageBody() id: number) {
    return this.leadManagementServiceService.remove(id);
  }
}
