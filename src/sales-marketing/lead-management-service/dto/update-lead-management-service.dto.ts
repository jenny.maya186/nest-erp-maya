import { PartialType } from '@nestjs/mapped-types';
import { CreateLeadManagementServiceDto } from './create-lead-management-service.dto';

export class UpdateLeadManagementServiceDto extends PartialType(CreateLeadManagementServiceDto) {
  id: number;
}
