import { Module } from '@nestjs/common';
import { CustomerRelationshipManagementModule } from './customer-relationship-management/customer-relationship-management.module';
import { SalesOrderManagementModule } from './sales-order-management/sales-order-management.module';
import { PriceAndDiscountManagementModule } from './price-and-discount-management/price-and-discount-management.module';
import { LeadManagementServiceModule } from './lead-management-service/lead-management-service.module';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [CustomerRelationshipManagementModule, SalesOrderManagementModule, PriceAndDiscountManagementModule, LeadManagementServiceModule]
})
export class SalesMarketingModule {}
