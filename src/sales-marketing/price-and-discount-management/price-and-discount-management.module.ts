import { Module } from '@nestjs/common';
import { PriceAndDiscountManagementService } from './price-and-discount-management.service';
import { PriceAndDiscountManagementGateway } from './price-and-discount-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [PriceAndDiscountManagementGateway, PriceAndDiscountManagementService],
})
export class PriceAndDiscountManagementModule {}
