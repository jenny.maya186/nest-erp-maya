import { Injectable } from '@nestjs/common';
import { CreatePriceAndDiscountManagementDto } from './dto/create-price-and-discount-management.dto';
import { UpdatePriceAndDiscountManagementDto } from './dto/update-price-and-discount-management.dto';

@Injectable()
export class PriceAndDiscountManagementService {
  create(createPriceAndDiscountManagementDto: CreatePriceAndDiscountManagementDto) {
    return 'This action adds a new priceAndDiscountManagement';
  }

  findAll() {
    return `This action returns all priceAndDiscountManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} priceAndDiscountManagement`;
  }

  update(id: number, updatePriceAndDiscountManagementDto: UpdatePriceAndDiscountManagementDto) {
    return `This action updates a #${id} priceAndDiscountManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} priceAndDiscountManagement`;
  }
}
