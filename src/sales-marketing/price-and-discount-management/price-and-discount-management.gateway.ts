import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { PriceAndDiscountManagementService } from './price-and-discount-management.service';
import { CreatePriceAndDiscountManagementDto } from './dto/create-price-and-discount-management.dto';
import { UpdatePriceAndDiscountManagementDto } from './dto/update-price-and-discount-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'price-discount-management' })
export class PriceAndDiscountManagementGateway extends BaseGateway {
  constructor(
    private readonly priceAndDiscountManagementService: PriceAndDiscountManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createPriceAndDiscountManagement')
  create(
    @MessageBody()
    createPriceAndDiscountManagementDto: CreatePriceAndDiscountManagementDto,
  ) {
    return this.priceAndDiscountManagementService.create(
      createPriceAndDiscountManagementDto,
    );
  }

  @SubscribeMessage('findAllPriceAndDiscountManagement')
  findAll() {
    return this.priceAndDiscountManagementService.findAll();
  }

  @SubscribeMessage('findOnePriceAndDiscountManagement')
  findOne(@MessageBody() id: number) {
    return this.priceAndDiscountManagementService.findOne(id);
  }

  @SubscribeMessage('updatePriceAndDiscountManagement')
  update(
    @MessageBody()
    updatePriceAndDiscountManagementDto: UpdatePriceAndDiscountManagementDto,
  ) {
    return this.priceAndDiscountManagementService.update(
      updatePriceAndDiscountManagementDto.id,
      updatePriceAndDiscountManagementDto,
    );
  }

  @SubscribeMessage('removePriceAndDiscountManagement')
  remove(@MessageBody() id: number) {
    return this.priceAndDiscountManagementService.remove(id);
  }
}
