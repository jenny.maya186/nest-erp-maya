import { PartialType } from '@nestjs/mapped-types';
import { CreatePriceAndDiscountManagementDto } from './create-price-and-discount-management.dto';

export class UpdatePriceAndDiscountManagementDto extends PartialType(CreatePriceAndDiscountManagementDto) {
  id: number;
}
