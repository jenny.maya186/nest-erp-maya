import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { CustomerRelationshipManagementService } from './customer-relationship-management.service';
import { CreateCustomerRelationshipManagementDto } from './dto/create-customer-relationship-management.dto';
import { UpdateCustomerRelationshipManagementDto } from './dto/update-customer-relationship-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'customer-relationship-management' })
export class CustomerRelationshipManagementGateway extends BaseGateway {
  constructor(
    private readonly customerRelationshipManagementService: CustomerRelationshipManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createCustomerRelationshipManagement')
  create(
    @MessageBody()
    createCustomerRelationshipManagementDto: CreateCustomerRelationshipManagementDto,
  ) {
    return this.customerRelationshipManagementService.create(
      createCustomerRelationshipManagementDto,
    );
  }

  @SubscribeMessage('findAllCustomerRelationshipManagement')
  findAll() {
    return this.customerRelationshipManagementService.findAll();
  }

  @SubscribeMessage('findOneCustomerRelationshipManagement')
  findOne(@MessageBody() id: number) {
    return this.customerRelationshipManagementService.findOne(id);
  }

  @SubscribeMessage('updateCustomerRelationshipManagement')
  update(
    @MessageBody()
    updateCustomerRelationshipManagementDto: UpdateCustomerRelationshipManagementDto,
  ) {
    return this.customerRelationshipManagementService.update(
      updateCustomerRelationshipManagementDto.id,
      updateCustomerRelationshipManagementDto,
    );
  }

  @SubscribeMessage('removeCustomerRelationshipManagement')
  remove(@MessageBody() id: number) {
    return this.customerRelationshipManagementService.remove(id);
  }
}
