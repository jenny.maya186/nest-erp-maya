import { Injectable } from '@nestjs/common';
import { CreateCustomerRelationshipManagementDto } from './dto/create-customer-relationship-management.dto';
import { UpdateCustomerRelationshipManagementDto } from './dto/update-customer-relationship-management.dto';

@Injectable()
export class CustomerRelationshipManagementService {
  create(createCustomerRelationshipManagementDto: CreateCustomerRelationshipManagementDto) {
    return 'This action adds a new customerRelationshipManagement';
  }

  findAll() {
    return `This action returns all customerRelationshipManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} customerRelationshipManagement`;
  }

  update(id: number, updateCustomerRelationshipManagementDto: UpdateCustomerRelationshipManagementDto) {
    return `This action updates a #${id} customerRelationshipManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} customerRelationshipManagement`;
  }
}
