import { Module } from '@nestjs/common';
import { CustomerRelationshipManagementService } from './customer-relationship-management.service';
import { CustomerRelationshipManagementGateway } from './customer-relationship-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [CustomerRelationshipManagementGateway, CustomerRelationshipManagementService],
})
export class CustomerRelationshipManagementModule {}
