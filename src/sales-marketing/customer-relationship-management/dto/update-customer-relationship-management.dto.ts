import { PartialType } from '@nestjs/mapped-types';
import { CreateCustomerRelationshipManagementDto } from './create-customer-relationship-management.dto';

export class UpdateCustomerRelationshipManagementDto extends PartialType(CreateCustomerRelationshipManagementDto) {
  id: number;
}
