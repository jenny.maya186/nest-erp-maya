import { Injectable } from '@nestjs/common';
import { CreateSalesOrderManagementDto } from './dto/create-sales-order-management.dto';
import { UpdateSalesOrderManagementDto } from './dto/update-sales-order-management.dto';

@Injectable()
export class SalesOrderManagementService {
  create(createSalesOrderManagementDto: CreateSalesOrderManagementDto) {
    return 'This action adds a new salesOrderManagement';
  }

  findAll() {
    return `This action returns all salesOrderManagement`;
  }

  findOne(id: number) {
    return `This action returns a #${id} salesOrderManagement`;
  }

  update(id: number, updateSalesOrderManagementDto: UpdateSalesOrderManagementDto) {
    return `This action updates a #${id} salesOrderManagement`;
  }

  remove(id: number) {
    return `This action removes a #${id} salesOrderManagement`;
  }
}
