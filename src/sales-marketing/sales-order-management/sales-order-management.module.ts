import { Module } from '@nestjs/common';
import { SalesOrderManagementService } from './sales-order-management.service';
import { SalesOrderManagementGateway } from './sales-order-management.gateway';

import { BaseGatewayModule } from 'src/base-gateway/base-gateway.module';

@Module({
  imports: [BaseGatewayModule],
  providers: [SalesOrderManagementGateway, SalesOrderManagementService],
})
export class SalesOrderManagementModule {}
