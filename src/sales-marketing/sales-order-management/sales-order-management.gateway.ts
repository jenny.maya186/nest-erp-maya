import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
} from '@nestjs/websockets';
import { BaseGateway } from 'src/base.gateway';
import { SalesOrderManagementService } from './sales-order-management.service';
import { CreateSalesOrderManagementDto } from './dto/create-sales-order-management.dto';
import { UpdateSalesOrderManagementDto } from './dto/update-sales-order-management.dto';
import { RedisService } from 'src/redis.service';
import { WsCustomService } from 'src/ws.service';

@WebSocketGateway({ cors: true, namespace: 'sales-order' })
export class SalesOrderManagementGateway extends BaseGateway {
  constructor(
    private readonly salesOrderManagementService: SalesOrderManagementService,
    redisService: RedisService,
    wsService: WsCustomService,
  ) {
    super(redisService, wsService);
  }

  @SubscribeMessage('createSalesOrderManagement')
  create(
    @MessageBody() createSalesOrderManagementDto: CreateSalesOrderManagementDto,
  ) {
    return this.salesOrderManagementService.create(
      createSalesOrderManagementDto,
    );
  }

  @SubscribeMessage('findAllSalesOrderManagement')
  findAll() {
    return this.salesOrderManagementService.findAll();
  }

  @SubscribeMessage('findOneSalesOrderManagement')
  findOne(@MessageBody() id: number) {
    return this.salesOrderManagementService.findOne(id);
  }

  @SubscribeMessage('updateSalesOrderManagement')
  update(
    @MessageBody() updateSalesOrderManagementDto: UpdateSalesOrderManagementDto,
  ) {
    return this.salesOrderManagementService.update(
      updateSalesOrderManagementDto.id,
      updateSalesOrderManagementDto,
    );
  }

  @SubscribeMessage('removeSalesOrderManagement')
  remove(@MessageBody() id: number) {
    return this.salesOrderManagementService.remove(id);
  }
}
