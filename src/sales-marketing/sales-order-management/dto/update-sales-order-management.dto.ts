import { PartialType } from '@nestjs/mapped-types';
import { CreateSalesOrderManagementDto } from './create-sales-order-management.dto';

export class UpdateSalesOrderManagementDto extends PartialType(CreateSalesOrderManagementDto) {
  id: number;
}
