import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { DataSource, DataSourceOptions } from 'typeorm';

const synchronize = process.env.NODE_ENV === 'production' ? false : true;
export const DBConfig: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => ({
    type: 'postgres',
    host: configService.get('PG_SERVER'),
    port: +configService.get('PG_PORT'),
    username: configService.get('PG_USERNAME'),
    password: configService.get('PG_PASSWORD'),
    database: configService.get('PG_DB'),
    entities: [],
    synchronize: synchronize,
    retryAttempts: Infinity,
    retryDelay: 3000,
    autoLoadEntities: true,
    cache: true,
    installExtensions: true,
    uuidExtension: 'pgcrypto',
    logging: true,
    logger: 'file',
    dateStrings: ['DATE', 'DATETIME', 'TIMESTAMP'],
  }),
  dataSourceFactory: async (options: DataSourceOptions) => {
    const dataSource = await new DataSource(options).initialize();
    return dataSource;
  },
};
