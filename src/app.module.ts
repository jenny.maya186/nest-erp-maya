import { Module } from '@nestjs/common';
import { CommonModule } from './common/common.module';
import { CoreModule } from './core/core.module';
import { FinanceModule } from './finance/finance.module';
import { HumanResourcesModule } from './human-resources/human-resources.module';
import { SalesMarketingModule } from './sales-marketing/sales-marketing.module';
import { SupplyChainManagementModule } from './supply-chain-management/supply-chain-management.module';
import { ManufacturingModule } from './manufacturing/manufacturing.module';
import { CustomerServiceModule } from './customer-service/customer-service.module';
import { ProjectManagementModule } from './project-management/project-management.module';
import { DBConfig } from './config/db.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CacheInterceptor, CacheModule } from '@nestjs/cache-manager';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ScheduleModule } from '@nestjs/schedule';
import { MulterModule } from '@nestjs/platform-express';
import { JwtModule } from '@nestjs/jwt';
import { BaseGatewayModule } from './base-gateway/base-gateway.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      cache: true,
    }),
    CacheModule.register({
      isGlobal: true,
    }),
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '7d' },
    }),
    CacheModule.register(),
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        dest: configService.get<string>('MULTER_DEST'),
      }),
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    TypeOrmModule.forRootAsync(DBConfig),
    CommonModule,
    CoreModule,
    FinanceModule,
    HumanResourcesModule,
    SalesMarketingModule,
    SupplyChainManagementModule,
    ManufacturingModule,
    CustomerServiceModule,
    ProjectManagementModule,
    BaseGatewayModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
  ],
})
export class AppModule {}
